-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-11-2020 a las 23:51:29
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `t-learning`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `LOGIN` (IN `USER` CHAR(50), IN `PASS` CHAR(50))  BEGIN
		DECLARE USU CHAR(50);
		DECLARE FIC	CHAR(50);
		DECLARE IDUSU INT(10);
		DECLARE IDFIC INT(10);
		DECLARE CON INT unsigned;

		SELECT usu_usuari,fic_codigo,usu_id,fic_id INTO @USU,@FIC,@IDUSU,@IDFIC FROM tbl_usuario
			INNER JOIN tbl_ficha on usu_ficid=fic_id
		WHERE usu_usuari=USER and usu_passwd=PASS;

		SELECT COUNT(log_id) INTO @CON FROM tbl_login WHERE log_ficid=@IDFIC;

		IF @CON<6 THEN
			INSERT INTO tbl_login (log_usuid,log_ficid) values (@IDUSU,@IDFIC);
			SET @RES="SI";
		ELSE
			SET @RES="NO";
		END IF;

		SELECT @USU AS 'User',@FIC AS 'Ficha',@CON  AS 'Conteo',@RES  AS 'Login';

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_area`
--

CREATE TABLE `tbl_area` (
  `are_id` int(10) NOT NULL,
  `are_nombre` varchar(60) NOT NULL,
  `are_fchcrt` timestamp NOT NULL DEFAULT current_timestamp(),
  `are_fchupd` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `are_sedid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_area`
--

INSERT INTO `tbl_area` (`are_id`, `are_nombre`, `are_fchcrt`, `are_fchupd`, `are_sedid`) VALUES
(1, 'Administracion', '2020-11-17 05:48:15', '2020-11-17 05:48:15', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_auditoria`
--

CREATE TABLE `tbl_auditoria` (
  `aud_id` int(20) NOT NULL,
  `aud_descrp` varchar(200) NOT NULL,
  `aud_accion` varchar(20) DEFAULT 'MOVIMIENTO',
  `aud_fchcrt` timestamp NOT NULL DEFAULT current_timestamp(),
  `aud_modid` int(10) NOT NULL,
  `aud_usuid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_ciudad`
--

CREATE TABLE `tbl_ciudad` (
  `ciu_id` int(10) NOT NULL,
  `ciu_nombre` varchar(60) NOT NULL,
  `ciu_fchcrt` timestamp NOT NULL DEFAULT current_timestamp(),
  `ciu_fchupd` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ciu_depid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_ciudad`
--

INSERT INTO `tbl_ciudad` (`ciu_id`, `ciu_nombre`, `ciu_fchcrt`, `ciu_fchupd`, `ciu_depid`) VALUES
(1, 'Cali', '2020-11-17 05:43:21', '2020-11-17 05:43:21', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_comentario_foro`
--

CREATE TABLE `tbl_comentario_foro` (
  `com_id` int(10) NOT NULL,
  `com_mensj` varchar(250) NOT NULL,
  `com_usunom` varchar(50) NOT NULL,
  `com_foro_id` int(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_comentario_foro`
--

INSERT INTO `tbl_comentario_foro` (`com_id`, `com_mensj`, `com_usunom`, `com_foro_id`) VALUES
(1, 'Hola como estas 2', 'Pablo', 1),
(2, 'Hola como estas', 'Paula', 2),
(6, 'asdas', 'asdasd', 1),
(7, 'asdas', 'asdasd', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_departamento`
--

CREATE TABLE `tbl_departamento` (
  `dep_id` int(10) NOT NULL,
  `dep_nombre` varchar(60) NOT NULL,
  `dep_fchcrt` timestamp NOT NULL DEFAULT current_timestamp(),
  `dep_fchupd` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dep_paiid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_departamento`
--

INSERT INTO `tbl_departamento` (`dep_id`, `dep_nombre`, `dep_fchcrt`, `dep_fchupd`, `dep_paiid`) VALUES
(1, 'Valle del Cauca', '2020-11-17 05:42:19', '2020-11-30 05:42:19', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_ficha`
--

CREATE TABLE `tbl_ficha` (
  `fic_id` int(10) NOT NULL,
  `fic_codigo` varchar(60) NOT NULL,
  `fic_progrm` varchar(60) NOT NULL,
  `fic_abrvtr` varchar(10) NOT NULL,
  `fic_fchcrt` timestamp NOT NULL DEFAULT current_timestamp(),
  `fic_fchupd` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_ficha`
--

INSERT INTO `tbl_ficha` (`fic_id`, `fic_codigo`, `fic_progrm`, `fic_abrvtr`, `fic_fchcrt`, `fic_fchupd`) VALUES
(1, '1907036', 'Analisis y Desarrollo de Sistema de Informacion', 'ADSI', '2020-11-17 05:50:32', '2020-11-17 05:50:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_foro`
--

CREATE TABLE `tbl_foro` (
  `foro_id` int(7) NOT NULL,
  `foro_titulo` varchar(200) NOT NULL DEFAULT '',
  `foro_mensaje` varchar(200) NOT NULL DEFAULT '',
  `foro_fecha_inicio` date NOT NULL,
  `foro_fecha_fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_foro`
--

INSERT INTO `tbl_foro` (`foro_id`, `foro_titulo`, `foro_mensaje`, `foro_fecha_inicio`, `foro_fecha_fin`) VALUES
(1, 'Prueba 121', 'Por favor comenta algo ', '2020-11-25', '2020-11-25'),
(2, 'Prueba 3', 'asdas', '2020-11-28', '2020-11-26'),
(5, 'Prueba 1159', 'asdasd', '2020-11-27', '2020-11-26'),
(9, 'asdas', 'asdasd', '2020-11-19', '2020-11-26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_login`
--

CREATE TABLE `tbl_login` (
  `log_id` int(10) NOT NULL,
  `log_fchcrt` timestamp NOT NULL DEFAULT current_timestamp(),
  `log_usuid` int(10) NOT NULL,
  `log_ficid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_modulo`
--

CREATE TABLE `tbl_modulo` (
  `mod_id` int(10) NOT NULL,
  `mod_descrp` varchar(50) NOT NULL,
  `mod_fchcrt` timestamp NOT NULL DEFAULT current_timestamp(),
  `mod_fchupd` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_pais`
--

CREATE TABLE `tbl_pais` (
  `pai_id` int(10) NOT NULL,
  `pai_nombre` varchar(60) NOT NULL,
  `pai_fchcrt` timestamp NOT NULL DEFAULT current_timestamp(),
  `pai_fchupd` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `pai_postal` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_pais`
--

INSERT INTO `tbl_pais` (`pai_id`, `pai_nombre`, `pai_fchcrt`, `pai_fchupd`, `pai_postal`) VALUES
(1, 'Colombia', '2020-11-16 05:41:08', '2020-12-01 00:41:08', '9192');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_persona`
--

CREATE TABLE `tbl_persona` (
  `per_id` int(10) NOT NULL,
  `per_nombre` varchar(60) NOT NULL,
  `per_aplldo` varchar(60) NOT NULL,
  `per_fchnac` timestamp NOT NULL DEFAULT current_timestamp(),
  `per_dirccn` varchar(99) NOT NULL,
  `per_correo` varchar(99) NOT NULL,
  `per_telfno` varchar(20) NOT NULL,
  `per_fchcrt` timestamp NOT NULL DEFAULT current_timestamp(),
  `per_fchupd` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `per_areid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_persona`
--

INSERT INTO `tbl_persona` (`per_id`, `per_nombre`, `per_aplldo`, `per_fchnac`, `per_dirccn`, `per_correo`, `per_telfno`, `per_fchcrt`, `per_fchupd`, `per_areid`) VALUES
(1, 'Mayerlin', 'Ortiz', '2020-11-17 05:51:49', 'Cra 42B #26-04', 'luz.mayerlin2549@gmail.com', '3175232591', '2020-11-17 05:51:49', '2020-11-17 05:51:49', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_responder`
--

CREATE TABLE `tbl_responder` (
  `res_id` int(10) NOT NULL,
  `res_mensj` varchar(250) NOT NULL,
  `res_usunom` varchar(50) NOT NULL,
  `res_com_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_sede`
--

CREATE TABLE `tbl_sede` (
  `sed_id` int(10) NOT NULL,
  `sed_nombre` varchar(60) NOT NULL,
  `sed_fchcrt` timestamp NOT NULL DEFAULT current_timestamp(),
  `sed_fchupd` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `sed_ciuid` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_sede`
--

INSERT INTO `tbl_sede` (`sed_id`, `sed_nombre`, `sed_fchcrt`, `sed_fchupd`, `sed_ciuid`) VALUES
(1, 'Centro Tecnológico Industrial ', '2020-11-16 05:43:35', '2020-11-30 05:43:35', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_tipo_usuario`
--

CREATE TABLE `tbl_tipo_usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_tipo_usuario`
--

INSERT INTO `tbl_tipo_usuario` (`id`, `nombre`) VALUES
(1, 'Administrador'),
(2, 'Aprendiz');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_usuario`
--

CREATE TABLE `tbl_usuario` (
  `usu_id` int(10) NOT NULL,
  `usu_usuari` varchar(60) NOT NULL,
  `usu_passwd` varchar(32) NOT NULL,
  `usu_fchcrt` timestamp NOT NULL DEFAULT current_timestamp(),
  `usu_fchupd` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `usu_ficid` int(10) NOT NULL,
  `usu_perid` int(10) NOT NULL,
  `tipo_usuario_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_usuario`
--

INSERT INTO `tbl_usuario` (`usu_id`, `usu_usuari`, `usu_passwd`, `usu_fchcrt`, `usu_fchupd`, `usu_ficid`, `usu_perid`, `tipo_usuario_id`) VALUES
(2, 'Nemucraft28', '2549', '2020-11-17 20:13:55', '2020-11-17 20:13:55', 1, 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_area`
--
ALTER TABLE `tbl_area`
  ADD PRIMARY KEY (`are_id`),
  ADD KEY `are_sedid` (`are_sedid`);

--
-- Indices de la tabla `tbl_auditoria`
--
ALTER TABLE `tbl_auditoria`
  ADD PRIMARY KEY (`aud_id`),
  ADD KEY `aud_usuid` (`aud_usuid`),
  ADD KEY `aud_modid` (`aud_modid`);

--
-- Indices de la tabla `tbl_ciudad`
--
ALTER TABLE `tbl_ciudad`
  ADD PRIMARY KEY (`ciu_id`),
  ADD KEY `ciu_depid` (`ciu_depid`);

--
-- Indices de la tabla `tbl_comentario_foro`
--
ALTER TABLE `tbl_comentario_foro`
  ADD PRIMARY KEY (`com_id`);

--
-- Indices de la tabla `tbl_departamento`
--
ALTER TABLE `tbl_departamento`
  ADD PRIMARY KEY (`dep_id`),
  ADD KEY `dep_paiid` (`dep_paiid`);

--
-- Indices de la tabla `tbl_ficha`
--
ALTER TABLE `tbl_ficha`
  ADD PRIMARY KEY (`fic_id`);

--
-- Indices de la tabla `tbl_foro`
--
ALTER TABLE `tbl_foro`
  ADD PRIMARY KEY (`foro_id`);

--
-- Indices de la tabla `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `log_usuid` (`log_usuid`),
  ADD KEY `log_ficid` (`log_ficid`);

--
-- Indices de la tabla `tbl_modulo`
--
ALTER TABLE `tbl_modulo`
  ADD PRIMARY KEY (`mod_id`);

--
-- Indices de la tabla `tbl_pais`
--
ALTER TABLE `tbl_pais`
  ADD PRIMARY KEY (`pai_id`);

--
-- Indices de la tabla `tbl_persona`
--
ALTER TABLE `tbl_persona`
  ADD PRIMARY KEY (`per_id`),
  ADD KEY `per_areid` (`per_areid`);

--
-- Indices de la tabla `tbl_responder`
--
ALTER TABLE `tbl_responder`
  ADD PRIMARY KEY (`res_id`);

--
-- Indices de la tabla `tbl_sede`
--
ALTER TABLE `tbl_sede`
  ADD PRIMARY KEY (`sed_id`),
  ADD KEY `sed_ciuid` (`sed_ciuid`);

--
-- Indices de la tabla `tbl_tipo_usuario`
--
ALTER TABLE `tbl_tipo_usuario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD PRIMARY KEY (`usu_id`),
  ADD KEY `usu_perid` (`usu_perid`),
  ADD KEY `usu_ficid` (`usu_ficid`),
  ADD KEY `tipo_usuario_id` (`tipo_usuario_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_area`
--
ALTER TABLE `tbl_area`
  MODIFY `are_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_auditoria`
--
ALTER TABLE `tbl_auditoria`
  MODIFY `aud_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_ciudad`
--
ALTER TABLE `tbl_ciudad`
  MODIFY `ciu_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_comentario_foro`
--
ALTER TABLE `tbl_comentario_foro`
  MODIFY `com_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `tbl_departamento`
--
ALTER TABLE `tbl_departamento`
  MODIFY `dep_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_ficha`
--
ALTER TABLE `tbl_ficha`
  MODIFY `fic_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_foro`
--
ALTER TABLE `tbl_foro`
  MODIFY `foro_id` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `tbl_login`
--
ALTER TABLE `tbl_login`
  MODIFY `log_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_modulo`
--
ALTER TABLE `tbl_modulo`
  MODIFY `mod_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbl_pais`
--
ALTER TABLE `tbl_pais`
  MODIFY `pai_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_persona`
--
ALTER TABLE `tbl_persona`
  MODIFY `per_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_responder`
--
ALTER TABLE `tbl_responder`
  MODIFY `res_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_sede`
--
ALTER TABLE `tbl_sede`
  MODIFY `sed_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `tbl_tipo_usuario`
--
ALTER TABLE `tbl_tipo_usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  MODIFY `usu_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tbl_area`
--
ALTER TABLE `tbl_area`
  ADD CONSTRAINT `tbl_area_ibfk_1` FOREIGN KEY (`are_sedid`) REFERENCES `tbl_sede` (`sed_id`);

--
-- Filtros para la tabla `tbl_auditoria`
--
ALTER TABLE `tbl_auditoria`
  ADD CONSTRAINT `tbl_auditoria_ibfk_1` FOREIGN KEY (`aud_usuid`) REFERENCES `tbl_usuario` (`usu_id`),
  ADD CONSTRAINT `tbl_auditoria_ibfk_2` FOREIGN KEY (`aud_modid`) REFERENCES `tbl_modulo` (`mod_id`);

--
-- Filtros para la tabla `tbl_ciudad`
--
ALTER TABLE `tbl_ciudad`
  ADD CONSTRAINT `tbl_ciudad_ibfk_1` FOREIGN KEY (`ciu_depid`) REFERENCES `tbl_departamento` (`dep_id`);

--
-- Filtros para la tabla `tbl_departamento`
--
ALTER TABLE `tbl_departamento`
  ADD CONSTRAINT `tbl_departamento_ibfk_1` FOREIGN KEY (`dep_paiid`) REFERENCES `tbl_pais` (`pai_id`);

--
-- Filtros para la tabla `tbl_login`
--
ALTER TABLE `tbl_login`
  ADD CONSTRAINT `tbl_login_ibfk_1` FOREIGN KEY (`log_usuid`) REFERENCES `tbl_usuario` (`usu_id`),
  ADD CONSTRAINT `tbl_login_ibfk_2` FOREIGN KEY (`log_ficid`) REFERENCES `tbl_ficha` (`fic_id`);

--
-- Filtros para la tabla `tbl_persona`
--
ALTER TABLE `tbl_persona`
  ADD CONSTRAINT `tbl_persona_ibfk_1` FOREIGN KEY (`per_areid`) REFERENCES `tbl_area` (`are_id`);

--
-- Filtros para la tabla `tbl_sede`
--
ALTER TABLE `tbl_sede`
  ADD CONSTRAINT `tbl_sede_ibfk_1` FOREIGN KEY (`sed_ciuid`) REFERENCES `tbl_ciudad` (`ciu_id`);

--
-- Filtros para la tabla `tbl_usuario`
--
ALTER TABLE `tbl_usuario`
  ADD CONSTRAINT `tbl_usuario_ibfk_1` FOREIGN KEY (`usu_perid`) REFERENCES `tbl_persona` (`per_id`),
  ADD CONSTRAINT `tbl_usuario_ibfk_2` FOREIGN KEY (`usu_ficid`) REFERENCES `tbl_ficha` (`fic_id`),
  ADD CONSTRAINT `tbl_usuario_ibfk_3` FOREIGN KEY (`usu_perid`) REFERENCES `tbl_persona` (`per_id`),
  ADD CONSTRAINT `tbl_usuario_ibfk_4` FOREIGN KEY (`usu_ficid`) REFERENCES `tbl_ficha` (`fic_id`),
  ADD CONSTRAINT `tbl_usuario_ibfk_5` FOREIGN KEY (`tipo_usuario_id`) REFERENCES `tbl_tipo_usuario` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
