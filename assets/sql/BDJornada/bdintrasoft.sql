-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-10-2020 a las 16:36:20
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdintrasoft`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblacceso`
--

CREATE TABLE `tblacceso` (
  `Acc_Id` int(11) NOT NULL,
  `Acc_Acceso` int(11) NOT NULL,
  `TblAprendizFicha_Apr_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblanuncio`
--

CREATE TABLE `tblanuncio` (
  `Anun_Id` int(11) NOT NULL,
  `Anun_Nombre` varchar(25) NOT NULL,
  `Anun_Cuerpo` varchar(150) NOT NULL,
  `Anun_Fecha_Creacion` date NOT NULL,
  `TblFicha_Fic_Id` int(11) NOT NULL,
  `TblEstado_Est_Id` int(11) NOT NULL,
  `TblUsuario_Usu_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblaprendizficha`
--

CREATE TABLE `tblaprendizficha` (
  `Apr_Id` int(11) NOT NULL,
  `Apr_Contraseña` varchar(20) NOT NULL,
  `Apr_UsuarioFicha` varchar(25) NOT NULL,
  `TblFicha_Fic_Id` int(11) NOT NULL,
  `Tbl_Estado_Est_Id` int(11) NOT NULL,
  `TblRol_Rol_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblestado`
--

CREATE TABLE `tblestado` (
  `Est_Id` int(11) NOT NULL,
  `Est_Estado` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblfase`
--

CREATE TABLE `tblfase` (
  `Fase_Id` int(11) NOT NULL,
  `Fase_Nombre` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblficha`
--

CREATE TABLE `tblficha` (
  `Fic_Id` int(11) NOT NULL,
  `Fic_NumeroFicha` varchar(15) NOT NULL,
  `Fic_FechaInicio` date NOT NULL,
  `Fic_FechaFin` date NOT NULL,
  `TblProgramaFormacion_Pro_IdProg` int(11) NOT NULL,
  `TblEstado_Est_Id` int(11) NOT NULL,
  `TblTipoJornada_TipJor_Id` int(11) NOT NULL,
  `TblModalidad_Mod_Id` int(11) NOT NULL,
  `TblTipoOferta_TipOfe_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblforo`
--

CREATE TABLE `tblforo` (
  `For_IdForo` int(11) NOT NULL,
  `For_NombreForo` varchar(50) NOT NULL,
  `For_CuerpoForo` varchar(250) NOT NULL,
  `For_Fecha` date NOT NULL,
  `TblUsuario_Usu_Id` int(11) NOT NULL,
  `TblEstado_Est_Id` int(11) NOT NULL,
  `TblFicha_Fic_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblhorario`
--

CREATE TABLE `tblhorario` (
  `Hor_Id` int(11) NOT NULL,
  `Hor_TrimestreInicio` date NOT NULL,
  `Hor_TrimestreFin` date NOT NULL,
  `Hor_TrimestreNumero` int(11) NOT NULL,
  `TblFicha_Fic_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblmaterialapoyo`
--

CREATE TABLE `tblmaterialapoyo` (
  `Mat_Id` int(11) NOT NULL,
  `Mat_Titulo` varchar(100) NOT NULL,
  `Mat_FechaCreacion` date NOT NULL,
  `Mat_Descripcion` varchar(250) NOT NULL,
  `TblUsuario_Usu_Id` int(11) NOT NULL,
  `TblFase_Fase_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblmodalidad`
--

CREATE TABLE `tblmodalidad` (
  `Mod_Id` int(11) NOT NULL,
  `Mod_Nombre` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblparticipacionforo`
--

CREATE TABLE `tblparticipacionforo` (
  `ParFor_Id` int(11) NOT NULL,
  `ParFor_Fecha` date NOT NULL,
  `ParFor_Participante` varchar(25) NOT NULL,
  `ParFor_Respuesta` varchar(150) NOT NULL,
  `TblForo_For_IdForo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblprogramaformacion`
--

CREATE TABLE `tblprogramaformacion` (
  `Pro_IdProg` int(11) NOT NULL,
  `Pro_NombreProg` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblrel_material_ficha`
--

CREATE TABLE `tblrel_material_ficha` (
  `MacFic_Id` int(11) NOT NULL,
  `TblFicha_Fic_Id` int(11) NOT NULL,
  `TblMaterialApoyo_Mat_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblrol`
--

CREATE TABLE `tblrol` (
  `Rol_Id` int(11) NOT NULL,
  `Rol_Nombre` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbltipoidentificacion`
--

CREATE TABLE `tbltipoidentificacion` (
  `TipIde_Id` int(11) NOT NULL,
  `TipIde_Nombre` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbltipojornada`
--

CREATE TABLE `tbltipojornada` (
  `TipJor_Id` int(11) NOT NULL,
  `TipJor_Nombre` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbltipooferta`
--

CREATE TABLE `tbltipooferta` (
  `TipOfe_Id` int(11) NOT NULL,
  `TipOfe_Nombre` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblusuario`
--

CREATE TABLE `tblusuario` (
  `Usu_Id` int(11) NOT NULL,
  `Usu_Nomb1` varchar(25) NOT NULL,
  `Usu_Nomb2` varchar(25) NOT NULL,
  `Usu_Ape1` varchar(25) NOT NULL,
  `Usu_Ape2` varchar(25) NOT NULL,
  `Usu_Contraseña` varchar(15) NOT NULL,
  `Usu_N°Identificacion` varchar(10) NOT NULL,
  `TblEstado_Est_Id` int(11) NOT NULL,
  `TblRol_Rol_Id` int(11) NOT NULL,
  `TblTipoIdentificacion_TipIde_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tblacceso`
--
ALTER TABLE `tblacceso`
  ADD PRIMARY KEY (`Acc_Id`),
  ADD KEY `TblAprendizFicha_Apr_Id` (`TblAprendizFicha_Apr_Id`);

--
-- Indices de la tabla `tblanuncio`
--
ALTER TABLE `tblanuncio`
  ADD PRIMARY KEY (`Anun_Id`),
  ADD KEY `TblEstado_Est_Id` (`TblEstado_Est_Id`),
  ADD KEY `TblFicha_Fic_Id` (`TblFicha_Fic_Id`),
  ADD KEY `TblUsuario_Usu_Id` (`TblUsuario_Usu_Id`);

--
-- Indices de la tabla `tblaprendizficha`
--
ALTER TABLE `tblaprendizficha`
  ADD PRIMARY KEY (`Apr_Id`),
  ADD KEY `TblFicha_Fic_Id` (`TblFicha_Fic_Id`),
  ADD KEY `TblRol_Rol_Id` (`TblRol_Rol_Id`),
  ADD KEY `Tbl_Estado_Est_Id` (`Tbl_Estado_Est_Id`);

--
-- Indices de la tabla `tblestado`
--
ALTER TABLE `tblestado`
  ADD PRIMARY KEY (`Est_Id`);

--
-- Indices de la tabla `tblfase`
--
ALTER TABLE `tblfase`
  ADD PRIMARY KEY (`Fase_Id`);

--
-- Indices de la tabla `tblficha`
--
ALTER TABLE `tblficha`
  ADD PRIMARY KEY (`Fic_Id`),
  ADD KEY `TblEstado_Est_Id` (`TblEstado_Est_Id`),
  ADD KEY `TblModalidad_Mod_Id` (`TblModalidad_Mod_Id`),
  ADD KEY `TblProgramaFormacion_Pro_IdProg` (`TblProgramaFormacion_Pro_IdProg`),
  ADD KEY `TblTipoJornada_TipJor_Id` (`TblTipoJornada_TipJor_Id`),
  ADD KEY `TblTipoOferta_TipOfe_Id` (`TblTipoOferta_TipOfe_Id`);

--
-- Indices de la tabla `tblforo`
--
ALTER TABLE `tblforo`
  ADD PRIMARY KEY (`For_IdForo`),
  ADD KEY `TblEstado_Est_Id` (`TblEstado_Est_Id`),
  ADD KEY `TblFicha_Fic_Id` (`TblFicha_Fic_Id`),
  ADD KEY `TblUsuario_Usu_Id` (`TblUsuario_Usu_Id`);

--
-- Indices de la tabla `tblhorario`
--
ALTER TABLE `tblhorario`
  ADD PRIMARY KEY (`Hor_Id`),
  ADD KEY `TblFicha_Fic_Id` (`TblFicha_Fic_Id`);

--
-- Indices de la tabla `tblmaterialapoyo`
--
ALTER TABLE `tblmaterialapoyo`
  ADD PRIMARY KEY (`Mat_Id`),
  ADD KEY `TblFase_Fase_Id` (`TblFase_Fase_Id`),
  ADD KEY `TblUsuario_Usu_Id` (`TblUsuario_Usu_Id`);

--
-- Indices de la tabla `tblmodalidad`
--
ALTER TABLE `tblmodalidad`
  ADD PRIMARY KEY (`Mod_Id`);

--
-- Indices de la tabla `tblparticipacionforo`
--
ALTER TABLE `tblparticipacionforo`
  ADD PRIMARY KEY (`ParFor_Id`),
  ADD KEY `TblForo_For_IdForo` (`TblForo_For_IdForo`);

--
-- Indices de la tabla `tblprogramaformacion`
--
ALTER TABLE `tblprogramaformacion`
  ADD PRIMARY KEY (`Pro_IdProg`);

--
-- Indices de la tabla `tblrel_material_ficha`
--
ALTER TABLE `tblrel_material_ficha`
  ADD PRIMARY KEY (`MacFic_Id`),
  ADD KEY `TblFicha_Fic_Id` (`TblFicha_Fic_Id`),
  ADD KEY `TblMaterialApoyo_Mat_Id` (`TblMaterialApoyo_Mat_Id`);

--
-- Indices de la tabla `tblrol`
--
ALTER TABLE `tblrol`
  ADD PRIMARY KEY (`Rol_Id`);

--
-- Indices de la tabla `tbltipoidentificacion`
--
ALTER TABLE `tbltipoidentificacion`
  ADD PRIMARY KEY (`TipIde_Id`);

--
-- Indices de la tabla `tbltipojornada`
--
ALTER TABLE `tbltipojornada`
  ADD PRIMARY KEY (`TipJor_Id`);

--
-- Indices de la tabla `tbltipooferta`
--
ALTER TABLE `tbltipooferta`
  ADD PRIMARY KEY (`TipOfe_Id`);

--
-- Indices de la tabla `tblusuario`
--
ALTER TABLE `tblusuario`
  ADD PRIMARY KEY (`Usu_Id`),
  ADD KEY `TblEstado_Est_Id` (`TblEstado_Est_Id`),
  ADD KEY `TblRol_Rol_Id` (`TblRol_Rol_Id`),
  ADD KEY `TblTipoIdentificacion_TipIde_Id` (`TblTipoIdentificacion_TipIde_Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tblacceso`
--
ALTER TABLE `tblacceso`
  MODIFY `Acc_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblanuncio`
--
ALTER TABLE `tblanuncio`
  MODIFY `Anun_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblaprendizficha`
--
ALTER TABLE `tblaprendizficha`
  MODIFY `Apr_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblestado`
--
ALTER TABLE `tblestado`
  MODIFY `Est_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblfase`
--
ALTER TABLE `tblfase`
  MODIFY `Fase_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblficha`
--
ALTER TABLE `tblficha`
  MODIFY `Fic_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblforo`
--
ALTER TABLE `tblforo`
  MODIFY `For_IdForo` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblhorario`
--
ALTER TABLE `tblhorario`
  MODIFY `Hor_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblmaterialapoyo`
--
ALTER TABLE `tblmaterialapoyo`
  MODIFY `Mat_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblmodalidad`
--
ALTER TABLE `tblmodalidad`
  MODIFY `Mod_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblparticipacionforo`
--
ALTER TABLE `tblparticipacionforo`
  MODIFY `ParFor_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblprogramaformacion`
--
ALTER TABLE `tblprogramaformacion`
  MODIFY `Pro_IdProg` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblrel_material_ficha`
--
ALTER TABLE `tblrel_material_ficha`
  MODIFY `MacFic_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblrol`
--
ALTER TABLE `tblrol`
  MODIFY `Rol_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbltipoidentificacion`
--
ALTER TABLE `tbltipoidentificacion`
  MODIFY `TipIde_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbltipojornada`
--
ALTER TABLE `tbltipojornada`
  MODIFY `TipJor_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tbltipooferta`
--
ALTER TABLE `tbltipooferta`
  MODIFY `TipOfe_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tblusuario`
--
ALTER TABLE `tblusuario`
  MODIFY `Usu_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tblacceso`
--
ALTER TABLE `tblacceso`
  ADD CONSTRAINT `tblacceso_ibfk_1` FOREIGN KEY (`TblAprendizFicha_Apr_Id`) REFERENCES `tblaprendizficha` (`Apr_Id`);

--
-- Filtros para la tabla `tblanuncio`
--
ALTER TABLE `tblanuncio`
  ADD CONSTRAINT `tblanuncio_ibfk_1` FOREIGN KEY (`TblEstado_Est_Id`) REFERENCES `tblestado` (`Est_Id`),
  ADD CONSTRAINT `tblanuncio_ibfk_2` FOREIGN KEY (`TblFicha_Fic_Id`) REFERENCES `tblficha` (`Fic_Id`),
  ADD CONSTRAINT `tblanuncio_ibfk_3` FOREIGN KEY (`TblUsuario_Usu_Id`) REFERENCES `tblusuario` (`Usu_Id`);

--
-- Filtros para la tabla `tblaprendizficha`
--
ALTER TABLE `tblaprendizficha`
  ADD CONSTRAINT `tblaprendizficha_ibfk_1` FOREIGN KEY (`TblFicha_Fic_Id`) REFERENCES `tblficha` (`Fic_Id`),
  ADD CONSTRAINT `tblaprendizficha_ibfk_2` FOREIGN KEY (`TblRol_Rol_Id`) REFERENCES `tblrol` (`Rol_Id`),
  ADD CONSTRAINT `tblaprendizficha_ibfk_3` FOREIGN KEY (`Tbl_Estado_Est_Id`) REFERENCES `tblestado` (`Est_Id`);

--
-- Filtros para la tabla `tblficha`
--
ALTER TABLE `tblficha`
  ADD CONSTRAINT `tblficha_ibfk_1` FOREIGN KEY (`TblEstado_Est_Id`) REFERENCES `tblestado` (`Est_Id`),
  ADD CONSTRAINT `tblficha_ibfk_2` FOREIGN KEY (`TblModalidad_Mod_Id`) REFERENCES `tblmodalidad` (`Mod_Id`),
  ADD CONSTRAINT `tblficha_ibfk_3` FOREIGN KEY (`TblProgramaFormacion_Pro_IdProg`) REFERENCES `tblprogramaformacion` (`Pro_IdProg`),
  ADD CONSTRAINT `tblficha_ibfk_4` FOREIGN KEY (`TblTipoJornada_TipJor_Id`) REFERENCES `tbltipojornada` (`TipJor_Id`),
  ADD CONSTRAINT `tblficha_ibfk_5` FOREIGN KEY (`TblTipoOferta_TipOfe_Id`) REFERENCES `tbltipooferta` (`TipOfe_Id`);

--
-- Filtros para la tabla `tblforo`
--
ALTER TABLE `tblforo`
  ADD CONSTRAINT `tblforo_ibfk_1` FOREIGN KEY (`TblEstado_Est_Id`) REFERENCES `tblestado` (`Est_Id`),
  ADD CONSTRAINT `tblforo_ibfk_2` FOREIGN KEY (`TblFicha_Fic_Id`) REFERENCES `tblficha` (`Fic_Id`),
  ADD CONSTRAINT `tblforo_ibfk_3` FOREIGN KEY (`TblUsuario_Usu_Id`) REFERENCES `tblusuario` (`Usu_Id`);

--
-- Filtros para la tabla `tblhorario`
--
ALTER TABLE `tblhorario`
  ADD CONSTRAINT `tblhorario_ibfk_1` FOREIGN KEY (`TblFicha_Fic_Id`) REFERENCES `tblficha` (`Fic_Id`);

--
-- Filtros para la tabla `tblmaterialapoyo`
--
ALTER TABLE `tblmaterialapoyo`
  ADD CONSTRAINT `tblmaterialapoyo_ibfk_1` FOREIGN KEY (`TblFase_Fase_Id`) REFERENCES `tblfase` (`Fase_Id`),
  ADD CONSTRAINT `tblmaterialapoyo_ibfk_2` FOREIGN KEY (`TblUsuario_Usu_Id`) REFERENCES `tblusuario` (`Usu_Id`);

--
-- Filtros para la tabla `tblparticipacionforo`
--
ALTER TABLE `tblparticipacionforo`
  ADD CONSTRAINT `tblparticipacionforo_ibfk_1` FOREIGN KEY (`TblForo_For_IdForo`) REFERENCES `tblforo` (`For_IdForo`);

--
-- Filtros para la tabla `tblrel_material_ficha`
--
ALTER TABLE `tblrel_material_ficha`
  ADD CONSTRAINT `tblrel_material_ficha_ibfk_1` FOREIGN KEY (`TblFicha_Fic_Id`) REFERENCES `tblficha` (`Fic_Id`),
  ADD CONSTRAINT `tblrel_material_ficha_ibfk_2` FOREIGN KEY (`TblMaterialApoyo_Mat_Id`) REFERENCES `tblmaterialapoyo` (`Mat_Id`);

--
-- Filtros para la tabla `tblusuario`
--
ALTER TABLE `tblusuario`
  ADD CONSTRAINT `tblusuario_ibfk_1` FOREIGN KEY (`TblEstado_Est_Id`) REFERENCES `tblestado` (`Est_Id`),
  ADD CONSTRAINT `tblusuario_ibfk_2` FOREIGN KEY (`TblRol_Rol_Id`) REFERENCES `tblrol` (`Rol_Id`),
  ADD CONSTRAINT `tblusuario_ibfk_3` FOREIGN KEY (`TblTipoIdentificacion_TipIde_Id`) REFERENCES `tbltipoidentificacion` (`TipIde_Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
