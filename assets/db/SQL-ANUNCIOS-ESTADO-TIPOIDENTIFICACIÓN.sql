-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-11-2020 a las 23:06:03
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_anuncio`
--

CREATE TABLE `tbl_anuncio` (
  `anu_id` int(5) NOT NULL,
  `anu_titl` varchar(45) NOT NULL,
  `anu_descrp` varchar(255) DEFAULT NULL,
  `anu_fechcr` date NOT NULL,
  `anu_fechfn` date NOT NULL,
  `anu_fichid` int(5) NOT NULL,
  `anu_usuid` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_anuncio`
--

INSERT INTO `tbl_anuncio` (`anu_id`, `anu_titl`, `anu_descrp`, `anu_fechcr`, `anu_fechfn`, `anu_fichid`, `anu_usuid`) VALUES
(1, 'Vocero', 'as', '2020-11-12', '2020-11-12', 1, 1),
(2, 'Matecho', 'Hola xd', '2020-11-14', '2020-11-15', 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_estado`
--

CREATE TABLE `tbl_estado` (
  `est_id` int(5) NOT NULL,
  `est_nombr` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_estado`
--

INSERT INTO `tbl_estado` (`est_id`, `est_nombr`) VALUES
(1, 'Activo'),
(2, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_tipdoc_usuario`
--

CREATE TABLE `tbl_tipdoc_usuario` (
  `tip_id` int(5) NOT NULL,
  `tip_tipidn` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tbl_tipdoc_usuario`
--

INSERT INTO `tbl_tipdoc_usuario` (`tip_id`, `tip_tipidn`) VALUES
(1, 'C.C'),
(2, 'C.E'),
(3, 'T.I');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tbl_anuncio`
--
ALTER TABLE `tbl_anuncio`
  ADD PRIMARY KEY (`anu_id`);

--
-- Indices de la tabla `tbl_estado`
--
ALTER TABLE `tbl_estado`
  ADD PRIMARY KEY (`est_id`);

--
-- Indices de la tabla `tbl_tipdoc_usuario`
--
ALTER TABLE `tbl_tipdoc_usuario`
  ADD PRIMARY KEY (`tip_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tbl_anuncio`
--
ALTER TABLE `tbl_anuncio`
  MODIFY `anu_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbl_estado`
--
ALTER TABLE `tbl_estado`
  MODIFY `est_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `tbl_tipdoc_usuario`
--
ALTER TABLE `tbl_tipdoc_usuario`
  MODIFY `tip_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
