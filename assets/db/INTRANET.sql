
  CREATE DATABASE `INTRASENAWUAKALA`;
  USE `INTRASENAWUAKALA`;

  /* 
  	 NOMBRE DE TABLAS INICIA CON TBL_
  	 NOMBRE DE TABLAS EN SINGULAR
  	 NOMBRE DE TABLAS SIN ESPACIO
  	 NOMBRE DE TABLAS REMPLAZAR ESPACIO POR _
  	 NOMBRE DE TABLAS COMPLEJOS NO USAR UNIONES
  	 NOMBRE DE CAMPOS DEBEN TENER 3 LETRAS DEL NOMBRE TABLA Y _ (USUARIO -> USU_ )
  	 NOMBRE DE CAMPOS 6 DIGITOS POSTERIOR AL _
  	 NOMBRE DE CAMPOS PODEMOS CONSERVAR LA PRIMERA Y ULTIMA VOCAL
  	 ATRIBUTOS DE CAMPOS --> NO SER TACAÑOS
  	 NOMBRES DE TABLA, CAMPOS Y ATRIBUTOS (MINUSCULAS)
  	 ATRIBUTO DE LONGITUD DE PRIMARY KEY == FOREAN KEY
  	 LAS FORANEA VAN DE ULTIMAS EN LA TABLA
  	 NOMBRE FORANEAS CONTIENE 6 DIGITOS DE LAS 2 TABLAS USU_PERid 
  */

  CREATE TABLE `tbl_login`
  (
  	log_id 	 		int(10) NOT NULL AUTO_INCREMENT,
	log_fchcrt		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	log_usuid		int(10) NOT NULL,
	log_ficid		int(10) NOT NULL,
	PRIMARY KEY 	(log_id)
  );

  CREATE TABLE `tbl_ficha`
  (
  	fic_id 	 		int(10) NOT NULL AUTO_INCREMENT,
  	fic_codigo		VARCHAR(60) NOT NULL,
  	fic_progrm		VARCHAR(60) NOT NULL,
  	fic_abrvtr		VARCHAR(10) NOT NULL,
	fic_fchcrt		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	fic_fchupd		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,	
	PRIMARY KEY 	(fic_id)
  );

  CREATE TABLE `tbl_usuario`
  (
  	usu_id 	 		int(10) NOT NULL AUTO_INCREMENT,
	usu_usuari		varchar(60) NOT NULL,
	usu_passwd		varchar(32) NOT NULL,
	usu_fchcrt		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	usu_fchupd		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	usu_ficid		int(10) NOT NULL,
	usu_perid		int(10) NOT NULL,
	PRIMARY KEY 	(usu_id)
  );

  CREATE TABLE `tbl_persona`
  (	
	per_id 	 		int(10) NOT NULL AUTO_INCREMENT,
	per_nombre		varchar(60) NOT NULL,
	per_aplldo		varchar(60) NOT NULL,
	per_fchnac		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	per_dirccn		varchar(99) NOT NULL,
	per_correo		varchar(99) NOT NULL,
	per_telfno		varchar(20) NOT NULL,
	per_fchcrt		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	per_fchupd		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	per_areid		int(10) NOT NULL,
	PRIMARY KEY 	(per_id)
  );

  CREATE TABLE `tbl_area`
  (	
	are_id 	 		int(10) NOT NULL AUTO_INCREMENT,
	are_nombre		varchar(60) NOT NULL,
	are_fchcrt		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	are_fchupd		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	are_sedid		int(10) NOT NULL,
	PRIMARY KEY 	(are_id)
  );

  CREATE TABLE `tbl_sede`
  (	
	sed_id 	 		int(10) NOT NULL AUTO_INCREMENT,
	sed_nombre		varchar(60) NOT NULL,
	sed_fchcrt		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	sed_fchupd		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	sed_ciuid		int(10) NOT NULL,
	PRIMARY KEY 	(sed_id)
  );

  CREATE TABLE `tbl_ciudad`
  (	
	ciu_id 	 		int(10) NOT NULL AUTO_INCREMENT,
	ciu_nombre		varchar(60) NOT NULL,
	ciu_fchcrt		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ciu_fchupd		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	ciu_depid		int(10) NOT NULL,
	PRIMARY KEY 	(ciu_id)
  );

  CREATE TABLE `tbl_departamento`
  (	
	dep_id 	 		int(10) NOT NULL AUTO_INCREMENT,
	dep_nombre		varchar(60) NOT NULL,
	dep_fchcrt		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dep_fchupd		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	dep_paiid		int(10) NOT NULL,
	PRIMARY KEY 	(dep_id)
  );

  CREATE TABLE `tbl_pais`
  (	
	pai_id 	 		int(10) NOT NULL AUTO_INCREMENT,
	pai_nombre		varchar(60) NOT NULL,
	pai_fchcrt		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	pai_fchupd		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	pai_postal		varchar(5) NOT NULL,
	PRIMARY KEY 	(pai_id)
  );

  CREATE TABLE `tbl_modulo`
  (	
	mod_id 	 		int(10) NOT NULL AUTO_INCREMENT,
	mod_descrp		varchar(50) NOT NULL,
	mod_fchcrt		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	mod_fchupd		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY 	(mod_id)
  );

  CREATE TABLE `tbl_auditoria`
  (	
	aud_id 	 		int(20) NOT NULL AUTO_INCREMENT,
	aud_descrp		varchar(200) NOT NULL,
	aud_accion		varchar(20) DEFAULT 'MOVIMIENTO',
	aud_fchcrt		TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	aud_modid		int(10) NOT NULL,
	aud_usuid		int(10) NOT NULL,
	PRIMARY KEY 	(aud_id)
  );


	ALTER TABLE `tbl_usuario`
	ADD FOREIGN KEY		(usu_perid) REFERENCES tbl_persona(per_id),
	ADD FOREIGN KEY		(usu_ficid) REFERENCES tbl_ficha(fic_id);

	ALTER TABLE `tbl_persona`
	ADD FOREIGN KEY		(per_areid) REFERENCES tbl_area(are_id);

	ALTER TABLE `tbl_area`
	ADD FOREIGN KEY		(are_sedid) REFERENCES tbl_sede(sed_id);

	ALTER TABLE `tbl_sede`
	ADD FOREIGN KEY		(sed_ciuid) REFERENCES tbl_ciudad(ciu_id);

	ALTER TABLE `tbl_ciudad`
	ADD FOREIGN KEY		(ciu_depid) REFERENCES tbl_departamento(dep_id);

	ALTER TABLE `tbl_departamento`
	ADD FOREIGN KEY		(dep_paiid) REFERENCES tbl_pais(pai_id);

	ALTER TABLE `tbl_auditoria`
	ADD FOREIGN KEY		(aud_usuid) REFERENCES tbl_usuario(usu_id),
	ADD FOREIGN KEY		(aud_modid) REFERENCES tbl_modulo(mod_id);

	ALTER TABLE `tbl_usuario`
	ADD FOREIGN KEY		(usu_perid) REFERENCES tbl_persona(per_id),
	ADD FOREIGN KEY		(usu_ficid) REFERENCES tbl_ficha(fic_id);

	ALTER TABLE `tbl_login`
	ADD FOREIGN KEY		(log_usuid) REFERENCES tbl_usuario(usu_id),
	ADD FOREIGN KEY		(log_ficid) REFERENCES tbl_ficha(fic_id);	


DELIMITER //
CREATE PROCEDURE LOGIN(IN USER CHAR(50),IN PASS CHAR(50))
BEGIN
		DECLARE USU CHAR(50);
		DECLARE FIC	CHAR(50);
		DECLARE IDUSU INT(10);
		DECLARE IDFIC INT(10);
		DECLARE CON INT unsigned;

		SELECT usu_usuari,fic_codigo,usu_id,fic_id INTO @USU,@FIC,@IDUSU,@IDFIC FROM tbl_usuario
			INNER JOIN tbl_ficha on usu_ficid=fic_id
		WHERE usu_usuari=USER and usu_passwd=PASS;

		SELECT COUNT(log_id) INTO @CON FROM tbl_login WHERE log_ficid=@IDFIC;


		IF @CON<6 THEN
			INSERT INTO tbl_login (log_usuid,log_ficid) values (@IDUSU,@IDFIC);
			SET @RES="SI";
		ELSE
			SET @RES="NO";
		END IF;

		SELECT @USU AS 'User',@FIC AS 'Ficha',@CON  AS 'Conteo',@RES  AS 'Login';

END //
DELIMITER ;