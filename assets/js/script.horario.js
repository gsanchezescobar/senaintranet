function UpdateHorario()
{
    var formData = new FormData();
    // console.log(document.getElementById('customFile').files[0])
    // formData.append("thefile", file);
    formData.append("ctrl", 'horario');
    formData.append("acti", 'Actualizar');
    formData.append("id", document.getElementById("id").value);
    formData.append("trimestre", document.getElementById("trimestre").value);
    formData.append("file", document.getElementById('customFile').files[0]);

    const ajax = new XMLHttpRequest(); // Ojo Se puede Llamar la funcion CrearAjax();
    ajax.open("POST",g_url + "views/main.php",true); // Se usa el Controlador General y su Accion
    ajax.onreadystatechange = function (){
        if( ajax.readyState == 4 ) // Estado 4 es DONE = TERMINADO
        {
            if( ajax.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
            {
                // result.innerHTML = ajax.responseText;
                // location.reload();
            }
            else { console.log("Ups, Me equivoque;"); }
        }
     };
    // ajax.setRequestHeader("Content-Type","multipart/form-data");
    ajax.send(formData);
}

function CambiarImagen() {
    const ajax = new XMLHttpRequest() // Ojo Se puede Llamar la funcion CrearAjax();
    ajax.open("POST", g_url + "views/main.php", true) // Se usa el Controlador General y su Accion
    var id = document.getElementById('trimestre').value
    ajax.onreadystatechange = function (){
        if( ajax.readyState == 4 ) // Estado 4 es DONE = TERMINADO
        {
            if( ajax.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
            {
                var response = JSON.parse(ajax.response)
                if (response[0].Hor_RutaImagen !== '') {
                    document.getElementById('imagen').src = g_imagen_path + 'horarios/' + response[0].Hor_RutaImagen    
                } else {
                    document.getElementById('imagen').src = 'https://lh3.googleusercontent.com/proxy/WGGzQoK1LO8PTHHpucDd0xCAPNkyUyYoMQMeRCZqvBgy6QdFne2hiEmLZHHLAzDkipyuoZFYETEOqbv9vt2cmYV4V-oItLIydyj3J_z_xyIvKQsc3uBH6FL04mWcYIksH0Mn'
                }
                
            }
            else { console.log("Error") }
        }
     };
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
    ajax.send("ctrl=horario&acti=Obtener&id="+id)
}

function CargarSelects()
{
    const ajax = new XMLHttpRequest() // Ojo Se puede Llamar la funcion CrearAjax();
    ajax.open("POST", g_url + "views/main.php", true) // Se usa el Controlador General y su Accion
    ajax.onreadystatechange = function (){
        if( ajax.readyState == 4 ) // Estado 4 es DONE = TERMINADO
        {
            if( ajax.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
            {
                var response = JSON.parse(ajax.response)
                var select = document.getElementById('trimestre')
                for (var i = 0; i < response.length; i++) {
                    var aTag = document.createElement('option')
                    aTag.setAttribute('value', response[i].Pro_IdProg)
                    aTag.innerHTML = response[i].Pro_NombreProg
                    select.appendChild(aTag)
                }
            }
            else { console.log("Error") }
        }
     };
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
    ajax.send("ctrl=programa&acti=Obtener")
}