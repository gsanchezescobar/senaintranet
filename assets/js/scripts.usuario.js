	function ObjAjax()
	{
		var xmlhttp=false;
 		try 	   {			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");	  } 
		catch (e)  { try 	  {	xmlhttp = new ActiveXObject("Microsoft.XMLHTTP"); }
 					catch (E) {	xmlhttp = false;  } }
 		if (!xmlhttp && typeof XMLHttpRequest!='undefined') 
  				   {			xmlhttp = new XMLHttpRequest();     	          }
		return xmlhttp;
	}


	function BorrarUsuario(id)
	{
		var result = document.getElementById('tview');

		const ajax = new XMLHttpRequest(); // Ojo Se puede Llamar la funcion CrearAjax();
		ajax.open("POST","main.php",true); // Se usa el Controlador General y su Accion
		ajax.onreadystatechange = function (){
												if( ajax.readyState == 4 ) // Estado 4 es DONE = TERMINADO
												{
													if( ajax.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
													{

														result.innerHTML = ajax.responseText;

													}
													else
													{
														console.log("Ups, Me equivoque;");
													}
												}
											 };

		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send("ctrl=usuario&acti=eliminar&id="+id);
	}


	function InsertUsuario()
	{
		var result = document.getElementById('tview');

		var usuario   = document.formusuario.usuario.value;
		var nombres   = document.formusuario.nombres.value;
		var area   	  = document.formusuario.area.value;
		var clave     = document.formusuario.clave.value;
		var estado    = document.formusuario.estado.value;

		const ajax = new XMLHttpRequest(); // Ojo Se puede Llamar la funcion CrearAjax();
		ajax.open("POST","main.php",true); // Se usa el Controlador General y su Accion
		ajax.onreadystatechange = function (){
												if( ajax.readyState == 4 ) // Estado 4 es DONE = TERMINADO
												{
													if( ajax.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
													{

														result.innerHTML = ajax.responseText;

													}
													else
													{
														console.log("Ups, Me equivoque;");
													}
												}
											 };

		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send("ctrl=usuario&acti=insertar&usuario="+usuario+"&nombres="+nombres+"&area="+area+"&clave="+clave+"&estado="+estado);
	}


	function Editar(id,usuario,nombres,area,clave,estado)
	{
		document.formusuario.id.value = id;
		document.formusuario.usuario.value = usuario;
		document.formusuario.nombres.value = nombres;
		document.formusuario.area.value = area;
		document.formusuario.clave.value = clave;
		document.formusuario.estado.value = estado;
		document.getElementById("btnguardar").value = "Actualizar";
		// Cambiar la propiedad DEL FORMULARIO desde javascript de ONSUBMIT() ONCLICK() CAMBIE  -> UPDATEUSUARIO() al boton guardar
	}


	function UpdateUsuario()
	{

		var result = document.getElementById('tview');
		var id   	  = document.formusuario.usuario.value;
		var usuario   = document.formusuario.usuario.value;
		var nombres   = document.formusuario.nombres.value;
		var area   	  = document.formusuario.area.value;
		var clave     = document.formusuario.clave.value;
		var estado    = document.formusuario.estado.value;

		const ajax = new XMLHttpRequest(); // Ojo Se puede Llamar la funcion CrearAjax();
		ajax.open("POST","main.php",true); // Se usa el Controlador General y su Accion
		ajax.onreadystatechange = function (){
												if( ajax.readyState == 4 ) // Estado 4 es DONE = TERMINADO
												{
													if( ajax.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
													{
														result.innerHTML = ajax.responseText;
														document.getElementById("btnguardar").value = "Guardar";
														// limpiar el formulario
														// document.getElementById("formusuario") --> onlick --> insertusuario()

													}
													else { console.log("Ups, Me equivoque;"); }
												}
											 };
		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send("ctrl=usuario&acti=actualizar&id="+id+"&usuario="+usuario+"&nombres="+nombres+"&area="+area+"&clave="+clave+"&estado="+estado);
	}

	function IniciarSesion()
	{
		var user   = document.getElementById("user").value;
		var password   = document.getElementById("password").value;

		const ajax = new XMLHttpRequest(); // Ojo Se puede Llamar la funcion CrearAjax();
		ajax.open("POST", g_url + "views/main.php",true); // Se usa el Controlador General y su Accion
		ajax.onreadystatechange = function (){
			if( ajax.readyState == 4 ) // Estado 4 es DONE = TERMINADO
			{
				if( ajax.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
				{
					console.log(ajax)
					var response = JSON.parse(ajax.response);
					if (response.estado === 'error') {
						alerta('error', 'error', 'credenciales no validos')
					} else {
						window.location = g_url	
					}
					// window.location = g_url
					// document.getElementById("form").submit();
					// result.innerHTML = ajax.responseText;

				}
				else
				{
					alerta('error', 'error', 'credenciales no validos')
				}
			}
		};
		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send("ctrl=usuario&acti=loginUser&usuario="+user+"&contraseña="+password);
	}