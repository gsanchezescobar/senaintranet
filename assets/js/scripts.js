
	function BorrarCiudad(Ciu_Id)
	{
		var result = document.getElementById('tview');

		const ajax = new XMLHttpRequest(); // Ojo Se puede Llamar la funcion CrearAjax();
		ajax.open("POST","main.php",true); // Se usa el Controlador General y su Accion
		ajax.onreadystatechange = function (){
												if( ajax.readyState == 4 ) // Estado 4 es DONE = TERMINADO
												{
													if( ajax.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
													{

														result.innerHTML = ajax.responseText;

													}
													else
													{
														console.log("Ups, Me equivoque;");
													}
												}
											 };

		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send("ctrl=ciudad&acti=Eliminar&Ciu_Id="+Ciu_Id);
	}


	function InsertCiudad()
	{
		var result = document.getElementById('tview');

		var nombre  = document.formciudad.nombre.value;
		var depar   = document.formciudad.Depar.value;




		const ajax = new XMLHttpRequest(); // Ojo Se puede Llamar la funcion CrearAjax();
		ajax.open("POST","main.php",true); // Se usa el Controlador General y su Accion
		ajax.onreadystatechange = function (){
												if( ajax.readyState == 4 ) // Estado 4 es DONE = TERMINADO
												{
													if( ajax.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
													{

														result.innerHTML = ajax.responseText;

													}
													else
													{
														console.log("Ups, Me equivoque;");
													}
												}
											 };

		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send("ctrl=ciudad&acti=insertar&Ciu_Nomciu="+nombre+"&Dep_Id="+depar);

	
	}


	function Editar(nombre,depar,id)
	{
		
		document.formciudad.nombre.value = nombre;
		document.formciudad.Depar.value  = depar;
		document.formciudad.id.value     = id;
		


		document.getElementById("btnguardar").value = "Actualizar";

	}


	function UpdateCiudad()
	{

		var result = document.getElementById('tview');

		var nombre   = document.formciudad.nombre.value;
		var depar    = document.formciudad.Depar.value
		var id  	 = document.formciudad.id.value;



		const ajax = new XMLHttpRequest(); 
		ajax.open("POST","main.php",true); 
		ajax.onreadystatechange = function (){
												if( ajax.readyState == 4 ) 
												{
													if( ajax.status == 200 )
													{
														result.innerHTML = ajax.responseText;
														document.getElementById("btnguardar").value = "Grabar";
														

													}
													else { console.log("Ups, Me equivoque;"); }
												}
											 };
		ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		ajax.send("ctrl=ciudad&acti=Actualizar&Ciu_Nomciu="+nombre+"&Dep_Id="+depar+"&Ciu_Id="+id);
	}

	function ValidarCiudad()
	{

		if(document.getElementById("btnguardar").value=="Grabar"){

			InsertCiudad();

		}else if(document.getElementById("btnguardar").value=="Actualizar"){

			UpdateCiudad();

		}

	}