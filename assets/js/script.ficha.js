


function BorrarFicha(id){

    const ajax = new XMLHttpRequest();
    ajax.open("POST", g_url + "views/main.php", true); // se usa el controlador general y su accion

    ajax.onreadystatechange = function () { 

        if (ajax.readyState == 4) { // estado Done  = terminado
            if (ajax.status == 200) { // estado Success = corrercto
                // result.innerHTML = ajax.responseText;
                alerta('success', 'Exito', 'Se han eliminado los datos')
                location.reload()
            }else{
                console.log("error de novato");
            }
        }
    };

    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send("ctrl=ficha&acti=Eliminar&id="+id);


}



function InsertFicha(){

    var codigo = document.getElementById("codigo").value;
    var programa = document.getElementById("programa").value;
    var jornada = document.getElementById("jornada").value;
    var modalidad = document.getElementById("modalidad").value;
    var oferta = document.getElementById("oferta").value;
    var fcreacion = document.getElementById("fcreacion").value;
    var ffinal = document.getElementById("ffinal").value;

    if (codigo === '' || programa === '' || jornada === '' || modalidad === '' || oferta === '' || fcreacion === '' || ffinal === '') {
        alerta('error', 'Error', 'No pueden haber datos vacios')
        return false;
    }

    const ajax = new XMLHttpRequest();
    ajax.open("POST", g_url + "views/main.php", true); // se usa el controlador general y su accion

    ajax.onreadystatechange = function () { 

        if (ajax.readyState == 4) { // estado Done  = terminado
            if (ajax.status == 200) { // estado Success = corrercto
                // result.innerHTML = ajax.responseText;
                alerta('success', 'Exito', 'Se han guardados los datos')
                window.location = g_url + "ficha"
            }else{
                alerta('error', 'Alerta', 'Error comuniquese con soporte')
            }
        }
    };

    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send(
        "ctrl=ficha&acti=insertar" + 
        "&codigo=" + codigo +
        "&programa=" + programa +
        "&jornada=" + jornada +
        "&modalidad=" + modalidad +
        "&oferta=" + oferta +
        "&fcreacion=" + fcreacion +
        "&ffinal=" + ffinal
    );
    
}

function Editar(id)
{
    // var result = document.getElementById('tview');

    const ajax = new XMLHttpRequest();
    ajax.open("POST", g_url + "views/main.php", true); // se usa el controlador general y su accion

    ajax.onreadystatechange = function () { 

        if (ajax.readyState == 4) { // estado Done  = terminado
            if (ajax.status == 200) { // estado Success = corrercto
                // result.innerHTML = ajax.responseText;
                var response = JSON.parse(ajax.response)
                document.getElementById("id").value = response[0].Fic_Id
                document.getElementById("codigo").value = response[0].Fic_NumeroFicha
                document.getElementById("programa").value = response[0].TblProgramaFormacion_Pro_IdProg
                document.getElementById("jornada").value = response[0].TblTipoJornada_TipJor_Id
                document.getElementById("modalidad").value = response[0].TblModalidad_Mod_Id
                document.getElementById("oferta").value = response[0].TblTipoOferta_TipOfe_Id
                document.getElementById("fcreacion").value = response[0].Fic_FechaInicio
                document.getElementById("ffinal").value = response[0].Fic_FechaFin
            }else{
                console.log("error de novato");
            }
        }
    };

    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send("ctrl=ficha&acti=ObtenerPorId&id="+id);
    
}


function UpdateFicha()
{

    var id = document.getElementById("id").value;
    var codigo = document.getElementById("codigo").value;
    var programa = document.getElementById("programa").value;
    var jornada = document.getElementById("jornada").value;
    var modalidad = document.getElementById("modalidad").value;
    var oferta = document.getElementById("oferta").value;
    var fcreacion = document.getElementById("fcreacion").value;
    var ffinal = document.getElementById("ffinal").value;

    if (codigo === '' || programa === '' || jornada === '' || modalidad === '' || oferta === '' || fcreacion === '' || ffinal === '') {
        alerta('error', 'Error', 'No pueden haber datos vacios')
        return false;
    }

    const ajax = new XMLHttpRequest();
    ajax.open("POST", g_url + "views/main.php", true); // se usa el controlador general y su accion

    ajax.onreadystatechange = function () { 

        if (ajax.readyState == 4) { // estado Done  = terminado
            if (ajax.status == 200) { // estado Success = corrercto
                // result.innerHTML = ajax.responseText;
                alerta('success', 'Exito', 'Se han guardados los datos')
                window.location = g_url + "index.php?view=ficha"
            }else{
                alerta('error', 'Alerta', 'Error comuniquese con soporte')
            }
        }
    };

    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    ajax.send(
        "ctrl=ficha&acti=actualizar" + 
        "&id=" + id +
        "&codigo=" + codigo +
        "&programa=" + programa +
        "&jornada=" + jornada +
        "&modalidad=" + modalidad +
        "&oferta=" + oferta +
        "&fcreacion=" + fcreacion +
        "&ffinal=" + ffinal
    );
}

function CargarSelects()
{
    const ajax = new XMLHttpRequest() // Ojo Se puede Llamar la funcion CrearAjax();
    ajax.open("POST", g_url + "views/main.php", true) // Se usa el Controlador General y su Accion
    ajax.onreadystatechange = function (){
        if( ajax.readyState == 4 ) // Estado 4 es DONE = TERMINADO
        {
            if( ajax.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
            {
                var response = JSON.parse(ajax.response)
                var select = document.getElementById('programa')
                for (var i = 0; i < response.length; i++) {
                    var aTag = document.createElement('option')
                    aTag.setAttribute('value', response[i].Pro_IdProg)
                    aTag.innerHTML = response[i].Pro_NombreProg
                    select.appendChild(aTag)
                }
            }
            else { console.log("Error") }
        }
     };
    ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
    ajax.send("ctrl=programa&acti=Obtener")

    // jornada
    const ajax2 = new XMLHttpRequest() // Ojo Se puede Llamar la funcion CrearAjax();
    ajax2.open("POST", g_url + "views/main.php", true) // Se usa el Controlador General y su Accion
    ajax2.onreadystatechange = function (){
        if( ajax2.readyState == 4 ) // Estado 4 es DONE = TERMINADO
        {
            if( ajax2.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
            {
                var response = JSON.parse(ajax2.response)
                var select = document.getElementById('jornada')
                for (var i = 0; i < response.length; i++) {
                    var aTag = document.createElement('option')
                    aTag.setAttribute('value', response[i].TipJor_Id)
                    aTag.innerHTML = response[i].TipJor_Nombre
                    select.appendChild(aTag)
                }
            }
            else { console.log("Error") }
        }
     };
    ajax2.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
    ajax2.send("ctrl=jornada&acti=Obtener")

    // modalidad
    const ajax3 = new XMLHttpRequest() // Ojo Se puede Llamar la funcion CrearAjax();
    ajax3.open("POST", g_url + "views/main.php", true) // Se usa el Controlador General y su Accion
    ajax3.onreadystatechange = function (){
        if( ajax3.readyState == 4 ) // Estado 4 es DONE = TERMINADO
        {
            if( ajax3.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
            {
                var response = JSON.parse(ajax3.response)
                var select = document.getElementById('modalidad')
                for (var i = 0; i < response.length; i++) {
                    var aTag = document.createElement('option')
                    aTag.setAttribute('value', response[i].Mod_Id)
                    aTag.innerHTML = response[i].Mod_Nombre
                    select.appendChild(aTag)
                }
            }
            else { console.log("Error") }
        }
     };
    ajax3.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
    ajax3.send("ctrl=modalidad&acti=Obtener")

    // oferta
    const ajax4 = new XMLHttpRequest() // Ojo Se puede Llamar la funcion CrearAjax();
    ajax4.open("POST", g_url + "views/main.php", true) // Se usa el Controlador General y su Accion
    ajax4.onreadystatechange = function (){
        if( ajax4.readyState == 4 ) // Estado 4 es DONE = TERMINADO
        {
            if( ajax4.status == 200 ) // Estado 200 es SUCCESS = CORRECTO
            {
                var response = JSON.parse(ajax4.response)
                var select = document.getElementById('oferta')
                for (var i = 0; i < response.length; i++) {
                    var aTag = document.createElement('option')
                    aTag.setAttribute('value', response[i].TipOfe_Id)
                    aTag.innerHTML = response[i].TipOfe_Nombre
                    select.appendChild(aTag)
                }
            }
            else { console.log("Error") }
        }
     };
    ajax4.setRequestHeader("Content-Type","application/x-www-form-urlencoded")
    ajax4.send("ctrl=oferta&acti=Obtener")
}