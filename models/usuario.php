<?php

	class Usuario
	{
		private $pdo;

		public function __Construct()
									 {
									 	try  				 {	$this->pdo=Database::Conectar(); }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }

		public function Select()
									 {
									 	try  				 {
										 							$sql=$this->pdo->prepare("SELECT * FROM tbl_usuario order by usu_id desc");
								 							$sql->execute();
									 							return $sql->fetchALL(PDO::FETCH_OBJ);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }


		public function Validate($user,$pass)
									 {
									 	try  				 {
				 							$sql=$this->pdo->prepare("SELECT 'USUARIO' as tipo, tblRol_Rol_Id, Rol_Nombre, Usu_Identificacion  as  identificacion FROM tblusuario
															INNER JOIN tblrol on TblRol_Rol_Id=Rol_Id
												            WHERE Usu_Identificacion=? and Usu_Contraseña=?
												            UNION
												SELECT 'FICHA'  as tipo, 0 as tblRol_Rol_Id, 'ficha' as Rol_Nombre, Fic_Id as  identificacion FROM tblficha
												            WHERE Fic_NumeroFicha=? and Fic_NumeroFicha=?
												            ");
									 							$sql->execute(array(
									 												$user,
									 												$pass,
									 												$user,
									 												$pass
									 												)
									 						 				 );
									 							return $sql->fetch(PDO::FETCH_OBJ);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }


		public function SelectUser($user,$pass)
									 {
									 	try  				 {
									 							$sql=$this->pdo->prepare("	SELECT usu_usuari,fic_codigo,are_nombre FROM tbl_usuario
															 								INNER JOIN tbl_ficha on usu_ficid=fic_id
															 								INNER JOIN tbl_persona on usu_perid=per_id
															 								INNER JOIN tbl_area on per_areid=are_id
															 								WHERE usu_usuari = ? and usu_passwd = ?");
									 							$sql->execute(array(
									 												$user,
									 												$pass
									 												)
									 						 				 );
									 							return $sql->fetch(PDO::FETCH_OBJ);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }


		public function Delete($id)
									 {
									 	try  				 {
									 							$sql="DELETE FROM tbl_usuario WHERE usu_id=?";
									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $id
									 									  			 	  )
									 									  			);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }


		public function Insert(Usuario $data)
									 {
									 	try  				 {
									 							$sql="INSERT INTO tbl_usuario(usuario,nombres,area,clave,estado) 
									 										        VALUES(?,?,?,?,?)";
									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $data->usuario,
									 									  			 	    $data->nombres,
									 									  			 	    $data->area,
									 									  			 	    md5($data->clave),
									 									  			 	    $data->estado
									 									  			 	  )
									 									  			);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }


		public function Update(Usuario $data)
									 {
									 	try  				 {
									 							$sql="UPDATE tbl_usuario
									 									 SET usuario = ?,
									 										 nombres = ?,
									 										 area    = ?,
									 										 clave   = ?,
									 										 estado  = ?
									 								  WHERE  id = ? ";

									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $data->usuario,
									 									  			 	    $data->nombres,
									 									  			 	    $data->area,
									 									  			 	    md5($data->clave),
									 									  			 	    $data->estado,
									 									  			 	    $data->id
									 									  			 	  )
									 									  			);
									 						 }

									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }


	}
?>