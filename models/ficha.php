<?php
    include(MODEL_PATH.'Database.php');
    class Ficha{
        private $pdo;
        private $id;

        public function __Construct(){
            try {
                $Database = new Database();
                $this->pdo = $Database->Conectar();
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function Select(){
            try {
                $sql=$this->pdo->prepare("SELECT 
                    *
                FROM tblficha tf
                INNER JOIN tblprogramaformacion tpf ON  tf.TblProgramaFormacion_Pro_IdProg = tpf.Pro_IdProg
                INNER JOIN tbltipoprograma ttp ON  tpf.Tip_Prog = ttp.Tip_Prog
                INNER JOIN tblestado te ON  tf.TblEstado_Est_Id = te.Est_Id
                INNER JOIN tbltipojornada ttj ON  tf.TblTipoJornada_TipJor_Id = ttj.TipJor_Id
                INNER JOIN tblmodalidad tm ON  tf.TblModalidad_Mod_Id = tm.Mod_Id
                INNER JOIN tbltipooferta tto ON  tf.TblTipoOferta_TipOfe_Id = tto.TipOfe_Id
                ");
                $sql->execute();
                return $sql->fetchALL(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());    
            }
        }

        public function FindById($id){
            try {
                $sql=$this->pdo->prepare("SELECT 
                    *
                FROM tblficha WHERE Fic_Id = ?
                ");
                $sql->execute(array($id));
                return $sql->fetchALL(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());    
            }
        }
    

        public function Delete($id){
            try {
                $sql= "DELETE FROM tblficha WHERE fic_id=?";
                $this->pdo->prepare($sql)->execute(array($id));

            
            } catch (Exception $e) {
                die($e->getMessage());    
            }
        }
        
        public function Create(Ficha $data){
            try {
                $sql= "INSERT INTO tblficha( Fic_NumeroFicha, TblProgramaFormacion_Pro_IdProg, TblTipoJornada_TipJor_Id, TblModalidad_Mod_Id, TblTipoOferta_TipOfe_Id, Fic_FechaInicio, Fic_FechaFin, TblEstado_Est_Id) VALUE(?,?,?,?,?,?,?,?)";
                $this->pdo->prepare($sql)->execute(array(
                    $data->codigo,$data->programa,$data->jornada,$data->modalidad,$data->oferta,$data->fcreacion,$data->ffinal,1));
                
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function Update($data)
        {
            try {
                $sql="UPDATE tblficha
                         SET Fic_NumeroFicha = ?,
                             TblProgramaFormacion_Pro_IdProg = ?,
                             TblTipoJornada_TipJor_Id    = ?,
                             TblModalidad_Mod_Id   = ?,
                             TblTipoOferta_TipOfe_Id  = ?,
                             Fic_FechaInicio = ?,
                             Fic_FechaFin = ?
                      WHERE  fic_id  = ? ";

                $this->pdo->prepare($sql)
                          ->execute(
                                     array(
                                            $data->codigo,
                                            $data->programa,
                                            $data->jornada,
                                            $data->modalidad,
                                            $data->oferta,
                                            $data->fcreacion,
                                            $data->ffinal,
                                            $data->id
                                          )
                                    );
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }





    }




?>