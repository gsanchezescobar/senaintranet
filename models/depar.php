
<?php

	class Depar
	{
		private $pdo; 


		public function __Construct()
									 {
									 	try  				 {	$this->pdo=Database::Conectar(); }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }

		public function Select()
									 {
									 	try  				 {
									 							$sql=$this->pdo->prepare("SELECT * FROM Tbl_Departamento order by Dep_Id desc");
									 							$sql->execute();
									 							return $sql->fetchALL(PDO::FETCH_OBJ);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }

		public function Delete($id)
									 {
									 	try  				 {
									 							$sql="DELETE FROM Tbl_Departamento WHERE Dep_Id=?";
									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $id
									 									  			 	  )
									 									  			);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }



		public function Insert(Depar $data)
									 {
									 	try  				 {
									 							$sql="INSERT INTO Tbl_Departamento(Dep_NomDpr,Pas_Id) 
									 										        VALUES(?,?)";
									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $data->nombre,
									 									  			 	    $data->pais	 
									 
									 									  			 	  )
									 									  			);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }

		public function Update(Depar $data)
									 {
									 	try  				 {
									 							$sql="UPDATE Tbl_Departamento
									 									 SET Dep_NomDpr = ?,
									 									     Pas_Id = ?	
									 					
									 								  WHERE  Dep_Id = ? ";

									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $data->nombre,
									 									  			 	    $data->pais,
									 									  			 	    $data->id
									 									  			 	  )
									 									  			);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }


	}
?>