<?php

// include(MODEL_PATH.'Database.php');
class Horario
{
	private $pdo;


	public function __construct(){
		try {	
			$Database = new Database();
            $this->pdo = $Database->Conectar();
		} catch (Exception $e) {	
			die($e->getMessage());			
		}
	}

	public function Select(){
		try {
			$sql=$this->pdo->prepare("SELECT * FROM tblhorario ORDER BY Hor_Id DESC");
			$sql->execute();
			return $sql->fetchALL(PDO::FETCH_OBJ);
		} catch (Exception $e) {	
			die($e->getMessage());
		}
	}

	public function SelectById($id){
		try {
			$sql=$this->pdo->prepare("SELECT * FROM tblhorario WHERE Hor_Id=$id");
			$sql->execute();
			return $sql->fetchALL(PDO::FETCH_OBJ);
		} catch (Exception $e) {	
			die($e->getMessage());
		}
	}

	public function Insert(Estado $data) {
		try {
			$sql="INSERT INTO tblhorario(Hor_TrimestreInicio, Hor_TrimestreFin, Hor_TrimestreNumero, TblFicha_Fic_Id)
						        VALUES(?,?,?,?)";
			$this->pdo->prepare($sql)
					  ->execute(
					  			 array(
					  			 	    $data->nombr
					  			 	   )
					  			);
		} catch (Exception $e) {	
			die($e->getMessage());
		}
	}

    public function Update($datos) {
		try {
			$sql="UPDATE tblhorario SET Hor_RutaImagen = ? WHERE Hor_Id = ?";
			$this->pdo->prepare($sql)
					  ->execute(
					  			 array(
					  			 	    $datos->name,
					  			 	    $datos->trimestre
					  			 	   )
					  			);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function Delete($id) {
		try {
			$sql="DELETE FROM tblhorario WHERE Hor_Id=?";
			$this->pdo->prepare($sql)
					  ->execute(
					  			 array(
					  			 	    $id
					  			 	  )
					  			);
		} catch (Exception $e) {	
			die($e->getMessage());
		}
	}



}


?>