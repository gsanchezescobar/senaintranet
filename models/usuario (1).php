<?php  
	class Usuario {

		private $pdo;


		public function __Construct(){ 

										try 					{	$this->pdo=Database::Conectar();	}
										catch (Exception $e) 	{	die($e->getMessage());				}
		}


/************************************************************** CONSULTAS DE FORO *****************************************************************/
		public function ComentarioSelect(){
										try 					{	
																	$sql=$this->pdo->prepare("SELECT * FROM tbl_foro ORDER BY foro_id desc");
																	$sql->execute();
																	return $sql->fetchALL(PDO::FETCH_OBJ);
																}

										catch (Exception $e) 	{	
																	die($e->getMessage());
																}

		}

		public function ComentarioInsertarBD(Usuario $data){
								
										try 					{	$sql = "INSERT INTO tbl_foro (foro_titulo, foro_mensaje, foro_fecha_inicio, foro_fecha_fin) 
																						  VALUES (?, ?, ?, ?)";
																	   $this->pdo->prepare($sql) 
																				 ->execute(
																							array(
																								$data->foro_titulo,
																								$data->foro_mensaje,
																								$data->foro_fecha_inicio,
																								$data->foro_fecha_fin
																							)
																						);
																		
												}

							    catch (Exception $e) { die($e->getMessage()); }

							} 

		public function Delete($id){
								
										try 					{	$sql="DELETE FROM usuarios WHERE id=?";
																	$this->pdo->prepare($sql)
										  					  				  ->execute(
										  												array(
										  					  									$id	
										  													)
										  												);
										  						}

										catch(exception $e)		{ die ($e->getMessage()); 				 }
		}
		

		public function Update(Usuario $data){
								
										try 					{	$sql = "UPDATE usuarios SET usuario=?, nombres=?, area=?, clave=?, estado=?
																						  WHERE id=?";
																	   $this->pdo->prepare($sql) 
																				 ->execute(
																							array(
																								$data->usuario,
																								$data->nombres,
																								$data->area,
																								md5($data->clave),
																								$data->estado,
																								$data->id
																							)
																						);
																		
												}

							    catch (Exception $e) { die($e->getMessage()); }

							} 

	}
?>