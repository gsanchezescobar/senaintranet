<?php

	class Ciudad
	{
		private $pdo;
	
		public function __Construct()
									 {
									 	try  				 {	$this->pdo=Database::Conectar(); }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }

		public function Select()
									 {
									 	try  				 {
									 							$sql=$this->pdo->prepare("SELECT * FROM Tbl_Ciudad order by Ciu_Id desc");
									 							$sql->execute();
									 							return $sql->fetchALL(PDO::FETCH_OBJ);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }

		public function Delete($id)
									 {
									 	try  				 {
									 							$sql="DELETE FROM Tbl_Ciudad WHERE Ciu_Id=?";
									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $id
									 									  			 	  )
									 									  			);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }



		public function Insert(Ciudad $data)
									 {
									 	try  				 {
									 							$sql="INSERT INTO Tbl_Ciudad(Ciu_Nomciu,Dep_Id) 
									 										        VALUES(?,?)";
									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $data->nombre,
									 									  			 	    $data->depar
									 									  			 	 
									 
									 									  			 	  )
									 									  			);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }


		public function Update(Ciudad $data)
									 {
									 	try  				 {
									 							$sql="UPDATE Tbl_Ciudad
									 									 SET Ciu_Nomciu = ?,
									 									     Dep_Id = ?	
									 					
									 								  WHERE  Ciu_Id = ? ";

									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $data->nombre,
									 									  			 	    $data->depar,
									 									  			 	    $data->id
									 									  			 	  )
									 									  			);
									 						 }

									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }

	}
?>