<?php

	class Sede
	{
		private $pdo;

		public function __Construct()
									 {
									 	try  				 {	$this->pdo=Database::Conectar(); }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }

		public function Select()
									 {
									 	try  				 {
									 							$sql=$this->pdo->prepare("SELECT * FROM Tbl_Sede order by Sed_Id desc");
									 							$sql->execute();
									 							return $sql->fetchALL(PDO::FETCH_OBJ);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }

		public function Delete($id)
									 {
									 	try  				 {
									 							$sql="DELETE FROM Tbl_Sede WHERE Sed_Id=?";
									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $id
									 									  			 	  )
									 									  			);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }



		public function Insert(Sede $data)
									 {
									 	try  				 {
									 							$sql="INSERT INTO Tbl_Sede(Sed_Nombre,Sed_Dircon,Ciu_Id) 
									 										        VALUES(?,?,?)";
									 							
									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $data->nombre,
									 									  			 	    $data->direccion,
									 									  			 	    $data->ciudad
									 									  			 	  )
									 									  			);
									 						 }
									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }

		public function Update(Sede $data)
									 {
									 	try  				 {
									 							$sql="UPDATE Tbl_Sede
									 									 SET Sed_Nombre = ?,
									 										 Sed_Dircon = ?,
									 										 Ciu_Id = ?
									 					
									 								  WHERE  Sed_Id = ?";
									 								 
									 							$this->pdo->prepare($sql)
									 									  ->execute(
									 									  			 array(
									 									  			 	    $data->nombre,
									 									  			 	    $data->direccion, 
									 									  			 	    $data->ciudad,
									 									  			 	    $data->id

									 									  			 	  )
									 									  			);
									 						 }

									 	catch (Exception $e) {	die($e->getMessage());			 }
									 }
	}
?>