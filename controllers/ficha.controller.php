<?php

require_once(MODEL_PATH.'ficha.php');

    class fichaController{
        private $ficha;

        function __Construct(){
            $this->ficha = new Ficha();
        }

        public function index(){

            //require_once('../views/frames/header.php');
            require_once(VIEW_PATH.'Ficha/fichaView.php');
            //require_once('../views/frames/footer.php');
        }

        public function Eliminar(){

            $this->ficha->Delete($_REQUEST['id']);
            return json_encode(array('estado' => 'correcto'));

        }

        public function crear(){

            //require_once('../views/frames/header.php');
            require_once(VIEW_PATH.'Ficha/fichaFormulario.php');
            //require_once('../views/frames/footer.php');
        }

        public function editar(){

            //require_once('../views/frames/header.php');
            require_once(VIEW_PATH.'Ficha/fichaFormulario.php');
            //require_once('../views/frames/footer.php');
        }
        
        public function Insertar(){

            $datos =  $this->ficha;

            $datos->codigo = $_REQUEST['codigo'];
            $datos->programa = $_REQUEST['programa'];
            $datos->jornada = $_REQUEST['jornada'];
            $datos->modalidad = $_REQUEST['modalidad'];
            $datos->oferta = $_REQUEST['oferta'];
            $datos->fcreacion = $_REQUEST['fcreacion'];
            $datos->ffinal = $_REQUEST['ffinal'];

            $this->ficha->Create($datos);

            return json_encode(array('estado' => 'correcto'));

        }

        public function ObtenerPorId(){

            // $datos =  $this->ficha;

            // $datos->id = $_REQUEST['id'];

            $datos = $this->ficha->FindById($_REQUEST['id']);

            return json_encode($datos);

        }



        public function Actualizar()
        {

            $datos =  $this->ficha;

            $datos->id = $_REQUEST['id'];
            $datos->codigo = $_REQUEST['codigo'];
            $datos->programa = $_REQUEST['programa'];
            $datos->jornada = $_REQUEST['jornada'];
            $datos->modalidad = $_REQUEST['modalidad'];
            $datos->oferta = $_REQUEST['oferta'];
            $datos->fcreacion = $_REQUEST['fcreacion'];
            $datos->ffinal = $_REQUEST['ffinal'];

            $this->ficha->Update($datos);

            return json_encode(array('estado' => 'correcto'));
        }





    }




?>