<?php

	require_once('../models/pais.php');

	class PaisController
	{	
		private $pais;

		function __Construct()	{
							  		$this->pais= new Pais(); 		
							  	}

		public function Index()
								{
									require_once('../views/pais/paisView.php');
								}

		public function Eliminar()
								{
									$this->pais->Delete($_REQUEST['Pas_Id']);
									require_once('../views/pais/paisSelect.php');
								}

		public function Insertar()
								{

									$datos= $this->pais;

									$datos->nombre = $_REQUEST['Pas_NomPas'];
							 

									$this->pais->Insert($datos);

									require_once('../views/pais/paisSelect.php');
								}

		public function Actualizar()
								{
									
									$datos= $this->pais;

									$datos->nombre = $_REQUEST['Pas_NomPas'];
									$datos->id 		= $_REQUEST['Pas_Id'];
									
					

									$this->pais->Update($datos);

									require_once('../views/pais/paisSelect.php');
								}

	}

?>