<?php

require_once(MODEL_PATH.'database.php');
require_once(MODEL_PATH.'noticia.php');

class NoticiaController
{	
	private $noticia;

	function __Construct()	{
						  		$this->noticia= new Noticia();
						  	}

	public function Index()
							{
								require_once(VIEW_PATH.'noticia/noticiaView.php');
							}

	public function Insertar()
							{

								$datos= $this->noticia;

								$datos->titulo 	= $_REQUEST['titulo'];
								$datos->descrp 	= $_REQUEST['descrp'];
								$datos->fchcre	= $_REQUEST['fchcre'];
								$datos->fchfin	= $_REQUEST['fchfin'];
								$datos->usuid 	= $_REQUEST['usuid'];
								$datos->ficid 	= $_REQUEST['ficid'];

								$this->noticia->Insert($datos);

								// require_once('../views/noticia/noticiaSelect.php');
							}

	public function Eliminar()
							{
								$this->noticia->Delete($_REQUEST['id']);
								// require_once('../views/noticia/noticiaSelect.php');
							}

}

?>