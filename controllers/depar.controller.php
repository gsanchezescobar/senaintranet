<?php

	require_once('../models/depar.php');

	class DeparController
	{	
		private $depar;

		function __Construct()	{
							  		$this->depar= new Depar(); 		
							  	}

		public function Index()
								{
									require_once('../views/depar/deparView.php');
								}

		public function Eliminar()
								{
									$this->depar->Delete($_REQUEST['Dep_Id']);
									require_once('../views/depar/deparSelect.php');
								}

		public function Insertar()
								{

									$datos= $this->depar;

									$datos->nombre = $_REQUEST['nombre'];
								

									$this->depar->Insert($datos);

									require_once('../views/depar/deparSelect.php');
								}

		public function Actualizar()
								{
									
									$datos= $this->depar;

									$datos->id 		= $_REQUEST['id'];
									$datos->nombre = $_REQUEST['nombre'];
						

									$this->depar->Update($datos);

									require_once('../views/depar/deparSelect.php');
								}

	}

?>