<?php
	session_start();

	include_once("usuario.controller.php");

	if( ISSET($_POST['usuario']) && ISSET($_POST['pass']))
	{
			$user=filter_input(INPUT_POST,'usuario',FILTER_SANITIZE_SPECIAL_CHARS);
			$pass=filter_input(INPUT_POST,'pass',FILTER_SANITIZE_SPECIAL_CHARS);

			if(ISSET($user) && ISSET($pass))
			{
					$objUser=new UsuarioController();
					$respuesta=$objUser->loginUser($user,$pass);

					if (isset($respuesta->Login) and $respuesta->Login=="NO")
					{
						echo "NO SE PUEDE POR QUE ESTA LLENO PARA ESTA FICHA";
					}
					elseif(isset($respuesta->User) and $respuesta->Login=="SI")
					{
						echo "el Usuario es: ".$respuesta->User."<br>";
						echo "el Ficha es: ".$respuesta->Ficha."<br>";
						echo "el Conteo es: ".$respuesta->Conteo."<br>";
						echo "el Respuesta es: ".$respuesta->Login."<br>";
					}
					else
					{
						echo "NO SE PUEDE POR CREDENCIALES ERRADAS";
						header("Location: ../index.php");
						exit();
					}
			}
			else
			{
				echo "variables user y pass vacias";
				header("Location: ../index.php");
				exit();
			}
	}
	else
	{
		echo "campos desde formulario vacios";
		header("Location: ../index.php");
		exit();
	}

?>