<?php

	require_once(MODEL_PATH.'database.php');
	require_once(MODEL_PATH.'usuario.php');

	class UsuarioController
	{	
		private $usuario;

		function __Construct()	{
							  		$this->usuario= new Usuario(); 		// Instancia de la Clase del Modelo Usuario
							  	}

		public function Index()
								{
									require_once(VIEW_PATH.'usuario/usuarioView.php');
								}

		public function Eliminar()
								{
									$this->usuario->Delete($_REQUEST['id']);
									require_once(VIEW_PATH.'usuario/usuarioSelect.php');
								}

		public function Insertar()
								{

									$datos= $this->usuario;

									$datos->usuario = $_REQUEST['usuario'];
									$datos->nombres = $_REQUEST['nombres'];
									$datos->area 	= $_REQUEST['area'];
									$datos->clave 	= $_REQUEST['clave'];
									$datos->estado 	= $_REQUEST['estado'];

									$this->usuario->Insert($datos);

									require_once(VIEW_PATH.'usuario/usuarioSelect.php');
								}

		public function Actualizar()
								{
									
									$datos= $this->usuario;

									$datos->id 		= $_REQUEST['id'];
									$datos->usuario = $_REQUEST['usuario'];
									$datos->nombres = $_REQUEST['nombres'];
									$datos->area 	= $_REQUEST['area'];
									$datos->clave 	= $_REQUEST['clave'];
									$datos->estado 	= $_REQUEST['estado'];

									$this->usuario->Update($datos);

									require_once(VIEW_PATH.'usuario/usuarioSelect.php');
								}

		public function getUser($user,$pass)
								{
									return $this->usuario->SelectUser($user,$pass);
								}

		public function loginUser()
								{
									$user = $_REQUEST['usuario'];
									$pass = $_REQUEST['contraseña'];
									// session_start();
									$data = $this->usuario->Validate($user,$pass);

									if ($data) {
										session_start();
										$_SESSION['user'] = json_encode($data);
										return json_encode(array('estado' => 'correcto'));
									} else {
										return json_encode(array('estado' => 'error'));
									}

									// return count($this->usuario->Validate($user,$pass));
								}


	}

?>