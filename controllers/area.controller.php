<?php

	require_once('../models/area.php');

	class AreaController
	{	
		private $area;

		function __Construct()	{
							  		$this->area= new Area(); 		// Instancia de la Clase del Modelo Usuario
							  	}

		public function Index()
								{
									require_once('../views/area/areaView.php');
								}

		public function Eliminar()
								{
									$this->area->Delete($_REQUEST['id']);
									require_once('../views/area/areaSelect.php');
								}

		public function Insertar()
								{

									$datos= $this->area;

									$datos->nombre 		= $_REQUEST['nombre'];
									$datos->sedid 		= $_REQUEST['sedid'];

									$this->area->Insert($datos);

									require_once('../views/area/areaSelect.php');
								}

		public function Actualizar()
								{
									
									$datos= $this->area;

									$datos->id 		= $_REQUEST['id'];
									$datos->nombre  = $_REQUEST['nombre'];
									$datos->sedid 	= $_REQUEST['sedid'];

									$this->area->Update($datos);

									require_once('../views/area/areaSelect.php');
								}								

	}

?>