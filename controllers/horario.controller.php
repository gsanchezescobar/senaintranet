<?php
	require_once(MODEL_PATH.'database.php');
	require_once(MODEL_PATH.'horario.php');

	class horarioController
	{	
		private $horario;

		function __construct()	{
	  		$this->horario= new Horario(); 		
	  	}

		public function Index()
		{
			require_once('./views/horario/horarioView.php');
		}

		public function Obtener()
		{
			$datos = $this->horario->SelectById($_REQUEST['id']);
			return json_encode($datos);
		}

		public function Eliminar()
		{
			$this->horario->Delete($_REQUEST['id']);
			require_once('./views/horario/horarioSelect.php');
		}
		public function Actualizar()
		{
			$datos = $this->horario;
			$datos->id 		= $_REQUEST['id'];
			$datos->trimestre 	= $_REQUEST['trimestre'];
			$tmpFile = $_FILES['file']['tmp_name'];
			$filename = $_FILES['file']['name'];
			$tmp = explode('.', $filename);
			$ext = end($tmp);
			$datos->name = $_REQUEST['id'].'-'.$_REQUEST['trimestre'].'.'.$ext;
		    $newFile = IMAGEN_PATH.'horarios/'.$_REQUEST['id'].'-'.$_REQUEST['trimestre'].'.'.$ext;
		    $result = move_uploaded_file($tmpFile, $newFile);
		    // echo $_FILES['pic']['name'];
		    if ($result) {
		        $this->horario->Update($datos);
		        echo json_encode(array('estado', 'correcto'));
		    } else {
		        echo json_encode(array('estado', 'error'));
		    }

			// $this->horario->Update($datos);
			// require_once('../views/horario/horarioSelect.php');

		}

		// public function Insertar()
		// {

		// 	$datos = $this->horario;

		// 	$datos->nombr = $_REQUEST['nombre'];

		// 	$this->horario->Insert($datos);

		// 	require_once('../views/horario/horarioSelect.php');
		// }

	}

?>