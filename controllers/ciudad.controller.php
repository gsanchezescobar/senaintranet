<?php

	require_once('../models/ciudad.php');
	require_once('../models/depar.php');

	class CiudadController
	{	
		private $ciudad;

		function __Construct()	{
							  		$this->ciudad = new Ciudad(); 
							  		$this->depar  = new Depar(); 		
							  	}

		public function Index()
								{
									require_once('../views/ciudad/ciudadView.php');
								}

		public function Eliminar()
								{
									$this->ciudad->Delete($_REQUEST['Ciu_Id']);

									require_once('../views/ciudad/ciudadSelect.php');
								}

		public function Insertar()
								{

									$datos= $this->ciudad;

									$datos->nombre = $_REQUEST['Ciu_Nomciu'];
									$datos->depar  = $_REQUEST['Dep_Id'];

							 

									$this->ciudad->Insert($datos);

									require_once('../views/ciudad/ciudadSelect.php');
								}

		public function Actualizar()
								{
									
									$datos= $this->ciudad;

									$datos->nombre = $_REQUEST['Ciu_Nomciu'];
									$datos->depar  = $_REQUEST['Dep_Id'];
									$datos->id 	   = $_REQUEST['Ciu_Id'];
									
					

									$this->ciudad->Update($datos);

									require_once('../views/ciudad/ciudadSelect.php');
								}

	}

?>