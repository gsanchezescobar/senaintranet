<?php

	require_once('../models/sede.php');
	require_once('../models/ciudad.php');

	class SedeController
	{	
		private $sede;
		private $ciudad;

		function __Construct()	{
							  		$this->sede= new Sede();
							  		$this->ciudad= new Ciudad(); 		
							  	}

		public function Index()
								{
									require_once('../views/sede/sedeView.php');
								}

		public function Eliminar()
								{
									$this->sede->Delete($_REQUEST['Sed_Id']);
								
									

									require_once('../views/sede/sedeSelect.php');
								}

		public function Insertar()
								{

									$datos= $this->sede;

									 $datos->nombre    = $_REQUEST['Sed_Nombre'];
									 $datos->direccion = $_REQUEST['Sed_Dircon'];
									 $datos->ciudad    = $_REQUEST['Ciu_Id'];


									$this->sede->Insert($datos);

									require_once('../views/sede/sedeSelect.php');
								}

		public function Actualizar()
								{
									
									$datos= $this->sede;

								
									  $datos->nombre    = $_REQUEST['Sed_Nombre'];
									 $datos->direccion = $_REQUEST['Sed_Dircon'];						
				                      $datos->ciudad    = $_REQUEST['Ciu_Id'];
				                     $datos->id 	      = $_REQUEST['Sed_Id'];


									$this->sede->Update($datos);

									require_once('../views/sede/sedeSelect.php');
								}

	}

?>