<?php
		require_once('../dirs.php');
		// require_once(MODEL_PATH.'database.php');
		$controller = 'usuario';
		if ( !ISSET($_REQUEST['ctrl']) )
		{
			require_once(CONTROLLER_PATH."$controller.controller.php");
			$controller = ucwords($controller).'Controller';  // -->  UsuarioController
			$controller = new $controller;
			$controller->Index();
		}
		else
		{
			$controller = strtolower($_REQUEST['ctrl']);
			$accion = ucwords(strtolower(ISSET($_REQUEST['acti']) ? $_REQUEST['acti'] : 'Index'));
			require_once(CONTROLLER_PATH."$controller.controller.php");
			$controller = ucwords($controller).'Controller';
			$controller = new $controller;
			echo call_user_func(array($controller,$accion));
		}
?>