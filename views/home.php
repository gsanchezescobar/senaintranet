<?php
	include(VIEW_PATH.'/header.php');
	include(CONTROLLER_PATH.'ficha.controller.php');
	include(CONTROLLER_PATH.'horario.controller.php');
	include(CONTROLLER_PATH.'noticia.controller.php');

	// Comiendo de la sesión
	// session_start();
	// // Guardar datos de sesión
	// if ($_SESSION) {
	// 	require_once(VIEW_PATH.'login.php');
	// } else {
	// 	echo "aca";
	// 	require_once(VIEW_PATH.'main.php');
	// }
	$view = (isset($_REQUEST['view'])) ? $_REQUEST['view'] : '';
	switch ($view) {
		// ficha
		case 'ficha':
			$ficha = new fichaController();
			$ficha->index();
			break;
		case 'fichaInsertar':
			$ficha = new fichaController();
			$ficha->crear();
			break;
		case 'fichaEditar':
			$ficha = new fichaController();
			$ficha->editar();
			break;
		case 'fichaMenu':
			echo "menu de ficha";
			break;
		case 'horario':
			$horario = new horarioController();
			$horario->index();
			break;
		case 'noticia':
			$noticia = new noticiaController();
			$noticia->index();
			break;
		case 'logout':
			session_destroy();
			header("Location: index.php");
			break;
		default:
			include(VIEW_PATH.'dashboard.php');
			break;
	}

	include(VIEW_PATH.'footer.php');
?>