<script src="./assets/js/script.horario.js"></script>
<div id="main" class="jumbotron jumbotron-fluid " >
    <div class="container">
        <div>
            <h1 class="display-0 text-center">Horario</h1>
            <hr class="my-1">
        </div>
    </div>
</div>

<div id="main">
    <div class="container" >
        <div id="tview">
            <form class="form-inline">
                <input type="hidden" name="id" id="id" value="<?php echo $_REQUEST['ficha']; ?>">
                <div class="form-group mb-2">
                    <label for="trimestre">Trimestre</label>
                    <select class="form-control" id="trimestre" onchange="CambiarImagen()">
                        <option value="">Seleccione una opcion</option>
                        <?php foreach($this->horario->Select() as $filas): ?>
                            <!-- <?php var_dump($filas) ?> -->
                            <?php if ($filas->TblFicha_Fic_Id === $_REQUEST['ficha']) { ?>
                                <option value="<?php echo $filas->Hor_Id; ?>"><?php echo $filas->Hor_TrimestreNumero; ?></option>
                            <?php };?>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <div class="custom-file">
                        <input type="file" id="customFile" lang="es">
                    </div>
                </div>
                <button type="button" value="Grabar" id="btnguardar" class="btn btn-primary mb-2" onclick="UpdateHorario()">Registar</button>
            </form>
            <div class="row">
                <img src="https://lh3.googleusercontent.com/proxy/WGGzQoK1LO8PTHHpucDd0xCAPNkyUyYoMQMeRCZqvBgy6QdFne2hiEmLZHHLAzDkipyuoZFYETEOqbv9vt2cmYV4V-oItLIydyj3J_z_xyIvKQsc3uBH6FL04mWcYIksH0Mn" id="imagen" class="img-fluid" alt="Responsive image">
            </div>  
        </div>
    </div>
</div>