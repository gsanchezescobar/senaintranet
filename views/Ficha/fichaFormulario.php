<script src="./assets/js/script.ficha.js"></script>
<div id="main" class="jumbotron jumbotron-fluid " >
    <div class="container">
        <div>
            <?php if($_REQUEST['view'] === 'fichaInsertar') { ?>
                <h1 class="display-0 text-center">Fichas Insertar</h1>
            <?php } else { ?>
                <h1 class="display-0 text-center">Fichas Editar</h1>
            <?php } ?>
            <hr class="my-1">
        </div>
        <div class="menu">
            <a class="btn btn-dark" href="ficha">Regresar</a>
        </div>
    </div>
</div>

<div id="main">
    <div class="container" >
        <div id="tview">
            <form action="" id="formCrear" name="formCrear" method="POST" target="_selft">
				<div class="form-group">

        			<input type="text" class="form-control" id="id" hidden>
        
					<label for="codigo">Codigo</label>
        			<input type="text" class="form-control" id="codigo">

        			<label for="programa">Programa</label>
        			<select class="form-control" id="programa">
				    	<option value="">Seleccione una opcion</option>
				    </select>
        
					<label for="jornada">Jornada</label>
        			<select class="form-control" id="jornada">
				    	<option value="">Seleccione una opcion</option>
				    </select>

					<label for="modalidad">Modalidad</label>
        			<select class="form-control" id="modalidad">
				    	<option value="">Seleccione una opcion</option>
				    </select>

				    <label for="oferta">Tipo Oferta</label>
        			<select class="form-control" id="oferta">
				    	<option value="">Seleccione una opcion</option>
				    </select>

					<label for="fcreacion">Fecha Creacion</label>
        			<input type="date" class="form-control" id="fcreacion">

        			<label for="ffinal">Fecha final</label>
					<input type="date" class="form-control" id="ffinal">
				</div>
				<div class="form-group">
                    <?php if($_REQUEST['view'] === 'fichaInsertar') { ?>
                        <button type="button" value="Grabar" id="btnguardar" class="btn btn-primary" onclick="InsertFicha()">Registar</button>
                    <?php } else { ?>
                        <button type="button" value="Actualizar" id="btnguardar" class="btn btn-primary" onclick="UpdateFicha()">Actualizar</button>
                    <?php } ?>
				</div>
			</form>
        </div>
    </div>
</div>

<script type="text/javascript">
	CargarSelects()
    <?php if($_REQUEST['view'] === 'fichaEditar') { ?>
        Editar(<?php echo $_REQUEST['ficha']; ?>)
    <?php } ?>
</script>