<?php
	include_once 'conexion.php';

	if(isset($_GET['mod_id'])){
		$id=(int) $_GET['mod_id'];

		$buscar_id=$con->prepare('SELECT * FROM tbl_modulo WHERE mod_id=:mod_id LIMIT 1');
		$buscar_id->execute(array(
			':mod_id'=>$id
		));
		$resultado=$buscar_id->fetch();
	}else{
		header('Location: index.php');
	}


	if(isset($_POST['guardar'])){
		$nombre=$_POST['mod_descrp'];
		

		if(!empty($descrp)){
			
				$consulta_update=$con->prepare(' UPDATE tbl_modulo SET  
					mod_descrp=:mod_descrp
					WHERE mod_id=:mod_id;'
				);
				$consulta_update->execute(array(
					':mod_descrp' => $descrp
				));
				header('Location: index.php');
			
		}else{
			echo "<script> alert('Los campos estan vacios');</script>";
		}
	}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>descripcion</title>
	<link rel="stylesheet" href="css/estilo.css">
</head>
<body>
	<div class="contenedor">
		<h2>Nueva descripcion</h2>
		<form action="" method="post">
			<div class="form-group">
				<input type="text" name="mod_descrp" placeholder="Descripcion" class="input__text">
			</div>
			<div class="btn__group">
				<a href="index.php" class="btn btn__danger">Cancelar</a>
				<input type="submit" name="guardar" value="Guardar" class="btn btn__primary">
			</div>
		</form>
	</div>
</body>
</html>