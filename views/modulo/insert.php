<?php 
	include_once 'conexion.php';
	
	if(isset($_POST['guardar'])){
		$descrp=$_POST['mod_descrp'];
	
//incompleto
		if(!empty($descrp) ){

			$consulta_insert=$con->prepare('INSERT INTO tbl_modulo(mod_descrp) VALUES(:mod_descrp)');
			$consulta_insert->execute(array(':mod_descrp' => $descrp));

			header('location: index.php');
			

		}
		else{
			
			echo "<script> alert('Los campos estan vacios');</script>";	
		}

	}


?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>descripcion</title>
	<link rel="stylesheet" href="css/estilo.css">
</head>
<body>
	<div class="contenedor">
		<h2>Nueva descripcion</h2>
		<form action="" method="post">
			<div class="form-group">
				<input type="text" name="mod_descrp" placeholder="Descripcion" class="input__text">
			</div>
			<div class="btn__group">
				<a href="index.php" class="btn btn__danger">Cancelar</a>
				<input type="submit" name="guardar" value="Guardar" class="btn btn__primary">
			</div>
		</form>
	</div>
</body>
</html>