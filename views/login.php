<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Login</title>
  <meta content="" name="description">
  <meta content="" name="keywords">
  <!-- Favicons -->
 

  <!-- Google Fonts -->
  
  <!-- Vendor CSS Files -->
  <link href="./assets/css/bootstrap.min.css" rel="stylesheet">
  <!-- <link href="icofont.min.css" rel="stylesheet"> -->
  <link href="./assets/css/boxicons.min.css" rel="stylesheet">
  <link href="./assets/css/venobox.css" rel="stylesheet">
  <link href="./assets/css/owl.carousel.min.css" rel="stylesheet">
  

  <!-- Template Main CSS File -->
  <link href="./assets/css/style-login.css" rel="stylesheet">

  <script type="text/javascript" src="http://localhost/senaintranet/general.js"></script>
  <script src="./assets/js/scripts.usuario.js"></script>

  
</head>

<body style="background-color: #FC7323;">

  <!-- ======= Mobile nav toggle button ======= -->
 

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container"><img src="./assets/images/image.png"  alt="Sena cdti " width="250" height="200"></div>
   





     <CENTER>
        <h2 style="color: white;"  >Inicio sesion</h2>
       <hr style="background-color: white;" width=250>
      </CENTER>


      
      
      <br>


                <form method="post" action="" id="form"><center>

<!-- 
                    <select class="form-control" name="select">
                      <option  value="value"selected>------------------Rol------------------</option> 
                    <option value="value1">Aprendiz</option> 
                    <option value="value2" >Instructor</option>
                    <option value="value3">Admin</option>
                    </select>
                    <br> -->
                    






                  <div class="">
                     
                      <input id="user" type="text"  placeholder="User" name="identificacion" class="form-control" required="">
                        
                  </div>
                <br>

                  <!-- input password -->
                <div class="">
                  
                      <input id="password" type="password" placeholder="Password" name="password" class="form-control" required="">
                  </i>
                     
                  </div>

                    <!-- boton de ingresar -->
                <br>
                <button style="background-color: #FC7323;" class="btn btn-success "   type="button" name="enviar" onclick="IniciarSesion()">Iniciar sesion</button> 
                 <hr style="background-color: white;" width=250>


                  </center>
                </form>


                
                 








    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex flex-column justify-content-center align-items-center">
    <div class="hero-container" data-aos="fade-in">

     <center> <h1>BIENVENIDO</h1>

      <p> S.M.G.A <span class="typed lead" data-typed-items="  Este aplicativo fue diseñado con el fin de servir<br> a nuestro compañero Sena y colaborar en el aprendizaje<br> Colaborativo "></span></p>
      </center>
    </div>





  </section><!-- End Hero -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; Sena <strong><span>Intranet</span></strong>
      </div>
      
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="./assets/vendor/jquery/jquery.min.js"></script>
  <script src="./assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="./assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="./assets/vendor/php-email-form/validate.js"></script>
  <script src="./assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="./assets/vendor/counterup/counterup.min.js"></script>
  <script src="./assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="./assets/vendor/venobox/venobox.min.js"></script>
  <script src="./assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="./assets/vendor/typed.js/typed.min.js"></script>
  <script src="./assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="./assets/js/main-login.js"></script>

</body>

</html>