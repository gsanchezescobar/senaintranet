<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SGMA</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/images/favicon.png" rel="icon">
  <link href="assets/images/image.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
  <script type="text/javascript" src="http://localhost/senaintranet/general.js"></script>
  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: iPortfolio - v2.0.2
  * Template URL: https://bootstrapmade.com/iportfolio-bootstrap-portfolio-websites-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Mobile nav toggle button ======= -->
  <button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="d-flex flex-column">

      <div class="profile">

        <!--imagen de user-->
        <img src="assets/images/image.png" alt="img" class="img-fluid rounded-circle">
        <h1 class="text-light"><a href="index.html">Nombre<!-- Nombre usuario--></a></h1>
        <div class="social-links mt-3 text-center">
          <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
          <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
          <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
          <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
          <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        </div>
      </div>

      <nav class="nav-menu">
        <ul>
          <li class="active"><a href="index.php" onclick="fRedirect('index.php');return false;"><i class="bx bx-home"></i> <span>Home</span></a></li>
          <?php
            $session = $_SESSION['user'];
            $user = json_decode($session, true);
            if (intval($user['tblRol_Rol_Id']) === 0) { // ficha
              echo "<li><a href='index.php?view=ficha' onclick=\"fRedirect('index.php?view=ficha');return false;\"><i class='bx bx-user'></i> <span>Ficha</span></a></li>";
              echo "<li><a href='index.php?view=horario' onclick=\"fRedirect('index.php?view=horario');return false;\"><i class='bx bx-user'></i> <span>Horario</span></a></li>";
            } else if (intval($user['tblRol_Rol_Id']) === 1) { // administrador
              echo "<li><a href='index.php?view=ficha' onclick=\"fRedirect('index.php?view=ficha');return false;\"><i class='bx bx-user'></i> <span>Ficha</span></a></li>";
            } else if (intval($user['tblRol_Rol_Id']) === 2) { // instructor
              echo "<li><a href='index.php?view=noticia' onclick=\"fRedirect('index.php?view=noticia');return false;\"><i class='bx bx-user'></i> <span>Noticias</span></a></li>";
            } 
            // <li><a href="#about"><i class="bx bx-user"></i> <span>About</span></a></li>
            // <li><a href="#resume"><i class="bx bx-file-blank"></i> <span>Resume</span></a></li>
            // <li><a href="#portfolio"><i class="bx bx-book-content"></i> Portfolio</a></li>
            // <li><a href="#services"><i class="bx bx-server"></i> Services</a></li>
            // <li><a href="#contact"><i class="bx bx-envelope"></i> Contact</a></li>
          ?>
          <li class="active"><a href="index.php?view=logout" onclick="fRedirect('index.php?view=logout');return false;"><i class="bx bx-window-close"></i> <span>Cerrar session</span></a></li>
        </ul>
      </nav><!-- .nav-menu -->
      <button type="button" class="mobile-nav-toggle d-xl-none"><i class="icofont-navigation-menu"></i></button>

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section class="d-flex flex-column justify-content-center align-items-center">
    <div class="container">