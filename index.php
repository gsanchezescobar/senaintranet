<?php
	include('dirs.php');

	// Comiendo de la sesión
	session_start();
	// Guardar datos de sesión
	if (isset($_SESSION['user'])) {
		require_once(VIEW_PATH.'home.php');
	} else {
		session_destroy();
		require_once(VIEW_PATH.'login.php');
	}
?>